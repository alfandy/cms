

// fungsi get kabupaten by provinsiinsi
function getKabupatenByProvinsi(datas) {
	var data = datas.value;
	var provinsi = $(".provinsi");
	
	$.ajax({
		// url : apps/master-wilayah/kabupaten/getKabupatenByProvinsi
		url: provinsi.attr('data-url'),
		type: 'GET',
		dataType: 'html',
		data: {data: data},
		beforeSend : function() {
			// body...
		},
		success : function(success) {
			$(".desaGet").html('<select class="form-control select22"></select>');
			$(".kecamatanGet").html('<select class="form-control select22"></select>');
			$(".kabupatenGet").html(success);
		},
		error : function () {
			
		}

	});
	
	
}

// fungsi get kecamatan by kabupaten
function getKecamatanByKabupaten(datas) {
	var data = datas.value;
	var kabupaten = $(".kabupaten");
	
	$.ajax({
		// url  : apps/master-wilayah/kecamatan/getKecamatanByKabupaten
		url: kabupaten.attr('data-url'),
		type: 'GET',
		dataType: 'html',
		data: {data: data},
		beforeSend : function() {
			// body...
		},
		success : function(success) {
			$(".desaGet").html('<select class="form-control select222"></select>');
			$(".kecamatanGet").html(success);
		},
		error : function () {
			
		}

	});
	
	
}

// fungsi get kecamatan by kabupaten
function getDesaByKecamatan(datas) {
	var data = datas.value;
	var kecamatan = $(".kecamatan");
	
	$.ajax({
		// url  : apps/master-wilayah/desa/getDesaByKecamatan
		url: kecamatan.attr('data-url'),
		type: 'GET',
		dataType: 'html',
		data: {data: data},
		beforeSend : function() {
			// body...
		},
		success : function(success) {
			$(".desaGet").html(success);
		},
		error : function () {
			
		}

	});
	
	
}


// fungsi get kecamatan by kabupaten user
function getKecamatanByKabupatenUser(datas) {
	var data = datas.value;
	var kabupaten = $(".kabupatenUser");
	
	$.ajax({
		// url  : apps/master-wilayah/kecamatan/getKecamatanByKabupaten
		url: kabupaten.attr('data-url'),
		type: 'GET',
		dataType: 'html',
		data: {data: data},
		beforeSend : function() {
			// body...
		},
		success : function(success) {
			$(".desaGetUser").html('<select class="form-control select222"></select>');
			$(".kecamatanGetUser").html(success);
		},
		error : function () {
			
		}

	});
	
	
}


// fungsi get kecamatan by kabupaten user
function getDesaByKecamatanUser(datas) {
	var data = datas.value;
	var kecamatan = $(".kecamatanUser");
	
	$.ajax({
		// url  : apps/master-wilayah/desa/getDesaByKecamatan
		url: kecamatan.attr('data-url'),
		type: 'GET',
		dataType: 'html',
		data: {data: data},
		beforeSend : function() {
			// body...
		},
		success : function(success) {
			$(".desaGetUser").html(success);
		},
		error : function () {
			
		}

	});
	
	
}



