function getNorekeningByKodeTransaksi(kdTransaksi){
        var databank = firebase.database().ref('transaction').child('proof').orderByKey().equalTo(kdTransaksi);
        databank.on('value',function( msg ){
            var val = msg.val();
            // console.log(val);
            if (val !== null) {

                var obj  = Object.keys(val);
                var namaPemilikAN = val[kdTransaksi].namaPemilik;
                var noRekeningAN = val[kdTransaksi].noRekening;
                var bankAN = val[kdTransaksi].bank.bank;

             console.log(bankAN);
                $("#getBukutTabungan"+kdTransaksi).html(`${bankAN} / ${noRekeningAN} / ${namaPemilikAN}`);
            }else{
                $("#getBukutTabungan"+kdTransaksi).html(`Kosong`);
            }
        });
    }