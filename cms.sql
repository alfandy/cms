-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2017 at 07:00 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `iduser` varchar(255) NOT NULL,
  `ts` int(11) NOT NULL,
  `tampil` int(11) NOT NULL,
  `hapus` int(11) NOT NULL,
  `idkategori` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `caption` varchar(255) NOT NULL,
  `gambar` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `judul`, `isi`, `iduser`, `ts`, `tampil`, `hapus`, `idkategori`, `count`, `caption`, `gambar`) VALUES
(1, 'Hutan Lindung', '<p>aaa</p>\r\n', 'admin', 1507266878, 1, 0, 5, 2, 'a', '1507266878-bbb.PNG'),
(2, 'Selamatkan Penerus Bangsaku dari PCC yang Terus Menggerogoti', '<p>a</p>\r\n', 'admin', 1507360968, 1, 0, 2, 3, '', '1507360968-bbb.PNG'),
(3, 'Hutan Lindung', '<p>aaa</p>\r\n', 'admin', 1507266878, 1, 0, 6, 8, 'a', '1507266878-bbb.PNG'),
(4, 'Selamatkan Penerus Bangsaku dari PCC yang Terus Menggerogoti', '<p>a</p>\r\n', 'admin', 1507360968, 1, 0, 1, 9, '', '1507360968-bbb.PNG'),
(5, 'Hutan Lindung', '<p>aaa</p>\r\n', 'admin', 1507266878, 1, 0, 3, 4, 'a', '1507266878-bbb.PNG'),
(6, 'Selamatkan Penerus Bangsaku dari PCC yang Terus Menggerogoti', '<p>a</p>\r\n', 'admin', 1507360968, 1, 0, 4, 9, '', '1507360968-bbb.PNG'),
(7, 'Hutan Lindung', '<p>aaa</p>\r\n', 'admin', 1507266878, 1, 0, 4, 0, 'a', '1507266878-bbb.PNG'),
(8, 'Selamatkan Penerus Bangsaku dari PCC yang Terus Menggerogoti', '<p>a</p>\r\n', 'admin', 1507360968, 1, 0, 1, 9, '', '1507360968-bbb.PNG'),
(9, 'a', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod<br />\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,<br />\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo<br />\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse<br />\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non<br />\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n', 'admin', 1507362356, 1, 0, 3, 21, '', '1507682490-22192805_10212147775401216_1610605893_n.png');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE `kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `status` int(2) DEFAULT NULL,
  `hapus` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kategori`
--

INSERT INTO `kategori` (`id`, `nama`, `status`, `hapus`) VALUES
(1, 'Trending Topic', 1, 0),
(2, 'Kriminal', 1, 0),
(3, 'Nasional', 1, 0),
(4, 'Humanis', 1, 0),
(5, 'Lalu Lintas', 1, 0),
(6, 'Informasi', 1, 0),
(7, 'Edukasi', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '#',
  `status` int(11) NOT NULL DEFAULT '1',
  `hapus` int(11) NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '0',
  `icon` varchar(255) DEFAULT NULL,
  `posisi` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `nama`, `url`, `status`, `hapus`, `parent`, `icon`, `posisi`) VALUES
(1, 'Kriminal', 'punyaku/kategori-berita/2/kriminal', 1, 0, 0, NULL, 0),
(3, 'Nasional', 'punyaku/kategori-berita/3/nasional', 1, 0, 0, NULL, 0),
(4, 'Humanis', 'punyaku/kategori-berita/4/humanis', 1, 0, 0, NULL, 0),
(5, 'Lalu Lintas', 'punyaku/kategori-berita/5/lalu-lintas', 1, 0, 0, NULL, 0),
(6, 'Tranding Topik', 'punyaku/kategori-berita/1/trending-topic', 1, 0, 0, NULL, 0),
(7, 'Informasi', 'punyaku/kategori-berita/6/informasi', 1, 0, 0, NULL, 0),
(8, 'Edukasi', 'punyaku/kategori-berita/7/edukasi', 1, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `runing`
--

CREATE TABLE `runing` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `status` int(11) DEFAULT NULL,
  `hapus` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `runing`
--

INSERT INTO `runing` (`id`, `nama`, `status`, `hapus`) VALUES
(1, 'Presiden RI : Terus Tingkatkan, Kepercayaan Publik terhadap Polri Mencapai 78 persen', 1, 0),
(2, 'Presiden minta Polri untuk Petakan Potensi Konflik dan Provokasi Jelang Tahun Politik', 1, 0),
(3, 'Presiden RI Joko Widodo Menjadi Irup Hari Kesaktian Pancasila', 1, 0),
(4, 'DivHumas Polri dan Netizen Membangun Budaya Positif Bermedia Sosial di Kepri', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `seting`
--

CREATE TABLE `seting` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `ket` text NOT NULL,
  `alamat` text NOT NULL,
  `nomor` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `fb` varchar(255) NOT NULL,
  `tw` varchar(255) NOT NULL,
  `g` varchar(255) NOT NULL,
  `ig` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seting`
--

INSERT INTO `seting` (`id`, `nama`, `ket`, `alamat`, `nomor`, `email`, `fb`, `tw`, `g`, `ig`) VALUES
(1, 'Multimedia News', '                          ', '                                                                                                        ', '', '', 'https://www.facebook.com/DivHumasPolri/', 'https://twitter.com/divhumaspolri?lang=en', '', 'https://www.instagram.com/divisihumaspolri/');

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id_user` varchar(255) NOT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(60) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `display_name` varchar(45) DEFAULT NULL,
  `last_pass_change` datetime DEFAULT NULL,
  `default_pass` varchar(60) DEFAULT NULL,
  `status` enum('0','1') DEFAULT NULL,
  `stm` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  `induk` int(11) NOT NULL,
  `jumlahlogin` int(11) NOT NULL,
  `tslogin` int(11) NOT NULL,
  `tslogout` int(11) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `moderator` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id_user`, `username`, `password`, `level`, `display_name`, `last_pass_change`, `default_pass`, `status`, `stm`, `sid`, `induk`, `jumlahlogin`, `tslogin`, `tslogout`, `ip`, `domain`, `moderator`) VALUES
('5', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'admin', '2016-04-25 18:27:24', 'abas1.', '1', 0, 0, 0, 0, 0, 0, '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_user_log`
--

CREATE TABLE `t_user_log` (
  `user_id` int(11) NOT NULL COMMENT 'Reference to t_user',
  `ip_addr` varchar(45) DEFAULT NULL,
  `browser` varchar(45) DEFAULT NULL,
  `version` varchar(45) DEFAULT NULL,
  `platform` varchar(45) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `date_login` datetime DEFAULT NULL,
  `success` enum('0','1') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(10) UNSIGNED NOT NULL,
  `keterangan` text NOT NULL,
  `durasi` tinytext NOT NULL,
  `ts` int(10) UNSIGNED NOT NULL,
  `domain` varchar(250) NOT NULL,
  `tampil` tinyint(1) UNSIGNED NOT NULL,
  `kategori` varchar(250) NOT NULL,
  `iduser` varchar(25) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `keterangan`, `durasi`, `ts`, `domain`, `tampil`, `kategori`, `iduser`) VALUES
(1, 'a', 'a', 1507688990, '', 1, '6', 'admin'),
(2, 'Hidup sehat', '2', 1507689383, '', 1, '6', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `runing`
--
ALTER TABLE `runing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seting`
--
ALTER TABLE `seting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `domain` (`domain`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `runing`
--
ALTER TABLE `runing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2487;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
