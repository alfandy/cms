<?php

/*
 * function that generate the action buttons edit, delete
 * This is just showing the idea you can use it in different view or whatever fits your needs
 */

function get_btn_sekolah($id) {
    $ci = & get_instance();
    $html = '<span class="actions">';
    $html .='<a href="'.site_url('apps/sekolah/detail/'.$id) .'" title="Detail"><i class="fa fa-eye"></i></a> &nbsp;';
    $html .='<a href="'. site_url('apps/sekolah/edit/'.$id).'" title="Edit"><i class="fa fa-edit"></i></a> &nbsp;';
    $html .='<a href="'.site_url('apps/sekolah/hapus/'.$id).'" onclick="return confirm(\'Apakah anda yakin ingin menghapus data ini ?\');"><i class="fa fa-trash"></i></a> &nbsp;';
    $html .= '</span>';

    return $html;
}

if (!function_exists('get_btn')){
    function get_btn($c,$id){
        $CI = & get_instance();
        $html = '<span class="actions">';
        $html .='<a href="'.site_url('apps/'.$c.'/detail/'.$id) .'" title="Detail"><i class="fa fa-eye"></i></a> &nbsp;';
        $html .='<a href="'. site_url('apps/'.$c.'/edit/'.$id).'" title="Edit"><i class="fa fa-edit"></i></a> &nbsp;';
        $html .='<a href="'.site_url('apps/'.$c.'/hapus/'.$id).'" onclick="return confirm(\'Apakah anda yakin ingin menghapus data ini ?\');"><i class="fa fa-trash"></i></a> &nbsp;';
        $html .= '</span>';

        return $html;
    }
}
