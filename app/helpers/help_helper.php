<?php 

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
if (!function_exists('diskon')) {
    function diskon($hargaBarangSatuan , $jumlah , $diskon){
        $hargaBarang = $hargaBarangSatuan * $jumlah;
        $hargaDiskon = ($hargaBarang * $diskon) / 100;
        $hargaBayar = $hargaBarang - $hargaDiskon; 
        return $hargaBayar;
    }
}
if (!function_exists('fotoGet')) {
    function fotoGet($foto , $ukuran){
        $size = array(
                '1' => 'small',
                '2' => 'medium',
                '3' => 'large'
            );
        $foto = str_replace("*", $size[$ukuran], $foto);
        return SERVER_UPLOAD.$foto;
    }
}

if (!function_exists('desaGet')) {
    function desaGet($id){
        $CI = & get_instance();
        $wilayah = $CI->rest->_get('desa/'.$id);
        if (!empty($wilayah)) {
            $data = $wilayah->kecamatan->kabupaten->kabupaten . ', ' .$wilayah->kecamatan->kecamatan . ', ' .$wilayah->desa;

        }else{
            $data = "Tidak Ada Data Desa";
        }
        return $data;
    }
}

if (!function_exists('kabupatenGet')) {
    function kabupatenGet($id){
        $CI = & get_instance();
        $wilayah = $CI->rest->_get('kabupaten/'.$id);
        if (!empty($wilayah)) {
            $data = $wilayah->kabupaten;

        }else{
            $data = "Tidak Ada Data Desa";
        }
        return $data;
    }
}

if (!function_exists('banyak')) {
    function banyak($url_api){
        $CI = & get_instance();
        $get = $CI->rest->_get($url_api);
            return count($get);
        

    }
}

if (!function_exists('drop_list_api')) {
	function drop_list_api($url_api, $id, $name, $string , $colom = NULL) {
        $CI = & get_instance();
        if ($colom == NULL) {
            $get = $CI->rest->_get($url_api);
            if (!empty($get)) {
                foreach ($get as $val) {
                    $list[''] = $string;
                    $list[$val->$id] = $val->$name;
                }
            } else {
                $list[''] = "Tidak ada data";
            }
        }

        else{
            $get = $CI->rest->_get($url_api);
            if (!empty($get->$colom)) {
                foreach ($get->$colom as $val) {
                    $list[''] = $string;
                    $list[$val->$id] = $val->$name;
                }
            } else {
                $list[''] = "Tidak ada data";
            }
            
        }

        return $list;
            
       
    }
}

if (!function_exists('hapusKarakter')) {
    function hapusKarakter($karakter){
        
    }
}

if (!function_exists('lama')) {
    function lama($ts , $bulan = NULL , $tahun = NULL){
        $diff   =  strtotime(date('Y-m-d H:i:s')) - substr($ts, 0 , 10); 
        $tahun  = floor($diff / (60 * 60 * 24 * 365)) ;
        $bulan   =floor($diff / (60 * 60 * 24));
        $jam    = floor($diff / (60 * 60));
        $menit60  = $diff - $jam * (60 * 60);
        $menit = floor( $menit60 / 60 );
        if ($bulan == NULL) {
            $data = $jam .  ' jam ' . $menit . ' menit';
        }elseif ($tahun == NULL) {
            // $data = $bulan .' Bulan ' . $jam .  ' Jam ' . $menit . ' Menit';
            $data = $jam .  ' jam ' . $menit . ' menit';
        }else{
            $data = $tahun.' Tahun ' .$bulan .' Bulan ' . $jam .  ' Jam ' . $menit . ' Menit';
        }
        return $data;
    }
}

if (!function_exists('ts')) {
    function ts($timestemp){
        $fix = substr($timestemp, 0, 10);
        $date = new DateTime();
        // $fix = 3991972322;
        $tgl_n = $date->setTimestamp($fix);
        // return $date->format('Y-m-d H:i:s');
        $tgl_pecah1 = explode(' ', $date->format('Y-m-d H:i:s'));

        return nama_hari($tgl_pecah1[0]).' '.  tgl_indo($tgl_pecah1[0]) .' '.$tgl_pecah1[1] ;
    }
}


// tgl_indo
if ( ! function_exists('tgl_slash'))
{
    function tgl_slash($tgl)
    {
        $ubah = gmdate($tgl, time()+60*60*8);
        $pecah = explode("/",$ubah);
        $tanggal = $pecah[1];
        $bulan = bulan($pecah[0]);
        $tahun = $pecah[2];
        return $tanggal.' '.$bulan.' '.$tahun;
    }
}
if ( ! function_exists('nama_hari_slash'))
{
    function nama_hari_slash($tanggal)
    {
        $ubah = gmdate($tanggal, time()+60*60*8);
        $pecah = explode("/",$ubah);
        $tgl = $pecah[1];
        $bln = $pecah[0];
        $thn = $pecah[2];

        $nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
        $nama_hari = "";
        if($nama=="Sunday") {$nama_hari="Minggu";}
        else if($nama=="Monday") {$nama_hari="Senin";}
        else if($nama=="Tuesday") {$nama_hari="Selasa";}
        else if($nama=="Wednesday") {$nama_hari="Rabu";}
        else if($nama=="Thursday") {$nama_hari="Kamis";}
        else if($nama=="Friday") {$nama_hari="Jumat";}
        else if($nama=="Saturday") {$nama_hari="Sabtu";}
        return $nama_hari;
    }
}
if ( ! function_exists('tgl_indo'))
{
    function tgl_indo($tgl)
    {
        $ubah = gmdate($tgl, time()+60*60*8);
        $pecah = explode("-",$ubah);
        $tanggal = $pecah[2];
        $bulan = bulan($pecah[1]);
        $tahun = $pecah[0];
        return $tanggal.' '.$bulan.' '.$tahun;
    }
}

if ( ! function_exists('bulan'))
{
    function bulan($bln)
    {
        switch ($bln)
        {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;
        }
    }
}

if ( ! function_exists('nama_hari'))
{
    function nama_hari($tanggal)
    {
        $ubah = gmdate($tanggal, time()+60*60*8);
        $pecah = explode("-",$ubah);
        $tgl = $pecah[2];
        $bln = $pecah[1];
        $thn = $pecah[0];

        $nama = date("l", mktime(0,0,0,$bln,$tgl,$thn));
        $nama_hari = "";
        if($nama=="Sunday") {$nama_hari="Minggu";}
        else if($nama=="Monday") {$nama_hari="Senin";}
        else if($nama=="Tuesday") {$nama_hari="Selasa";}
        else if($nama=="Wednesday") {$nama_hari="Rabu";}
        else if($nama=="Thursday") {$nama_hari="Kamis";}
        else if($nama=="Friday") {$nama_hari="Jumat";}
        else if($nama=="Saturday") {$nama_hari="Sabtu";}
        return $nama_hari;
    }
}
if ( ! function_exists('hitung_mundur'))
{
    function hitung_mundur($wkt)
    {
        $waktu=array(   365*24*60*60    => "tahun",
                        30*24*60*60     => "bulan",
                        7*24*60*60      => "minggu",
                        24*60*60        => "hari",
                        60*60           => "jam",
                        60              => "menit",
                        1               => "detik");
        $hitung = strtotime(gmdate ("Y-m-d H:i:s", time () +60 * 60 * 8))-$wkt;
        $hasil = array();
        if($hitung<5)
        {
            $hasil = 'kurang dari 5 detik yang lalu';
        }
        else
        {
            $stop = 0;
            foreach($waktu as $periode => $satuan)
            {
                if($stop>=6 || ($stop>0 && $periode<60)) break;
                $bagi = floor($hitung/$periode);
                if($bagi > 0)
                {
                    $hasil[] = $bagi.' '.$satuan;
                    $hitung -= $bagi*$periode;
                    $stop++;
                }
                else if($stop>0) $stop++;
            }
            $hasil=implode(' ',$hasil).' yang lalu';
        }
        return $hasil;
    }
}

 ?>