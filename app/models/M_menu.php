<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_menu extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('menu', 'id');
    }

    public function get_all_s($hapus = ''){
    	$data = $this->db->select('*')
    			->from('menu')
    			->where(array('hapus' => $hapus ))
    			->get();
    	return $data->result();
    }

    public function get_aktif($start = 0 , $limit = 5 , $return = 'result')
    {
        $data = $this->db->select('*')
                ->from('menu')
                ->where(array('hapus' => '0'  , 'status' => '1' ))
                ->limit($limit , $start)
                ->get();
        return $data->$return();
    }
}