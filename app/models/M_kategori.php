<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_kategori extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('kategori', 'id');
    }

    function cari_like($like , $limit = 10){
       
        $data = $this->db->select('*')
                ->from('kategori')
                ->where(array('hapus' => '' ))
                ->like('nama' , $like)
                ->limit($limit)
                ->get();
        return $data->result();
    }

    public function get_all_s($hapus = ''){
    	$data = $this->db->select('*')
    			->from('kategori')
    			->where(array('hapus' => $hapus ))
    			->get();
    	return $data->result();
    }

    public function get_aktif()
    {
        $data = $this->db->select('*')
                ->from('kategori')
                ->where(array('hapus' => '0'  , 'status' => '1' ))
                ->get();
        return $data->result();
    }
}