<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_video extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('video', 'id');
    }

    public function get_page($limit, $start) {
        $this->db->limit($limit, $start);
        return $this->get_all_v_k();
    }

    public function get_all_v_k()
    {
        $d = $this->db->select('a.* , b.nama as nama_kategori ')
                ->from('video a')
                ->join('kategori b' , 'a.kategori = b.id' , 'left')
                ->order_by('a.ts' ,'desc')
                ->get();
        return $d->result();
    }

    public function get_all_tampil_limit($limit = 5 , $return = "result"){
        $d = $this->db->select('*')
            ->from('video')
            ->where(array('tampil' => '1'))
            ->order_by('ts', 'desc')
            ->limit($limit)
            ->get();
        return $d->$return();
    }

    public function get_all_v_l($id ,$limit)
    {
        $d = $this->db->select('a.* , b.nama as nama_kategori , c.nama as video , c.tanggal')
                ->from('video a')
                ->join('kategori b' , 'a.kategori = b.id' , 'left')
                ->join('fm c' , 'a.ts = c.id' , 'left' )
                ->where('a.setuju' , '1')
                ->where('a.id !=', $id)
                ->limit($limit)
                ->order_by('a.id' ,'desc')
                ->get();
        return $d->result();
    }

    public function get_all_v()
    {
    	$d = $this->db->select('a.* , b.nama as nama_kategori , c.nama as video')
    			->from('video a')
    			->join('kategori b' , 'a.kategori = b.id' , 'left')
    			->join('fm c' , 'a.ts = c.id' , 'left' )
    			->order_by('a.id' ,'desc')
    			->get();
    	return $d->result();
    }

    public function get_by_v($data)
    {
    	$d = $this->db->select('a.* , b.nama as nama_kategori ')
    			->from('video a')
    			->join('kategori b' , 'a.kategori = b.id' , 'left')
    			->where($data)
    			->get();
    	return $d->row();
    }

    

}
