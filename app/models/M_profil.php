<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_profil extends MY_Model {

    public function __construct() {
        parent::__construct();
        parent::set_table('profil', 'id');
    }

    public function get_all_s(){
    	$data = $this->db->select('*')
    			->from('profil')
    			->where(array('hapus' => '0' ))
    			->get();
    	return $data->result();
    }

    function cari_like($like , $limit = 10){
       
        $data = $this->db->select('a.*')
                ->from('profil a')
                ->where(array('a.hapus' => '0' ))
                ->like('a.judul' , $like)
                // ->or_like('a.caption' , $like )
                ->limit($limit)
                ->order_by('a.ts' , 'desc')
                ->get();
        return $data->result();
    }

    public function get_all_r()
    {
        $data = $this->db->select('a.*')
                ->from('profil a')
                ->where(array('a.hapus' => '0' ))
                // ->limit(1 , 0)
                ->order_by('a.ts' , 'desc')
                ->get();
        return $data->result();
    }

    public function get_aktif($start = 0 , $limit = 3 , $return = "result")
    {
        $data = $this->db->select('a.* ')
                ->from('profil a')
                ->where(array('a.hapus' => '0' , 'a.tampil' => '1'))
                ->limit($limit , $start)
                ->order_by('a.ts' , 'desc')
                ->get();
        return $data->$return();
    }

    public function get_berita_kategori($kategori , $start = 0 , $limit = 2 , $return = 'result'){
        $data = $this->db->select('a.* , b.nama as kategori')
                ->from('berita a')
                ->join('kategori b' , 'a.idkategori = b.id' , 'left')
                ->where(array('a.hapus' => '0' , 'a.tampil' => '1' , 'a.idkategori' => $kategori))
                ->limit($limit , $start)
                ->order_by('a.ts' , 'desc')
                ->get();
        return $data->$return();
    }
    public function get_berita_like($like , $start = 0 , $limit = 2 , $return = 'result'){
        $data = $this->db->select('a.* , b.nama as kategori')
                ->from('berita a')
                ->join('kategori b' , 'a.idkategori = b.id' , 'left')
                ->where(array('a.hapus' => '0' , 'a.tampil' => '1'))
                ->like('a.judul' ,$like )
                ->limit($limit , $start)
                ->order_by('a.ts' , 'desc')
                ->get();
        return $data->$return();
    }

}