<?php

defined('BASEPATH') OR exit('No direct script access allowed');

defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

defined('CLIENT_ID') OR define('CLIENT_ID', 'deezkon');
defined('CLIENT_SECRET') OR define('CLIENT_SECRET', '1sampe10');
defined('SERVER_PORT_AUTH') OR define('SERVER_PORT_AUTH', '8172');
defined('SERVER_PORT_APP') OR define('SERVER_PORT_APP', '8171');
defined('SERVER_MAIN') OR define('SERVER_MAIN','35.186.156.18');
// defined('SERVER_MAIN') OR define('SERVER_MAIN', '192.168.2.129');
defined('SERVER_LOGIN') OR define('SERVER_LOGIN', 'http://' . SERVER_MAIN . ':' . SERVER_PORT_AUTH . '/');
defined('SERVER_APP') OR define('SERVER_APP', 'http://' . SERVER_MAIN . ':' . SERVER_PORT_APP . '/api/');
defined('GRANT_TYPE') OR define('GRANT_TYPE', 'password');
defined('AUTHORIZTION') OR define('AUTHORIZTION', base64_encode(CLIENT_ID . ':' . CLIENT_SECRET));
defined('SERVER_UPLOAD') OR define('SERVER_UPLOAD','http://files.deezkon.com');
