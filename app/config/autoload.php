<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$autoload['packages'] = array();
$autoload['libraries'] = array('database','form_validation','upload','autentifikasi','session','parser');
$autoload['drivers'] = array();
$autoload['helper'] = array('url','form','inflector','text','security','cookie','help','smart');
$autoload['config'] = array();
$autoload['language'] = array();
$autoload['model'] = array(
	"m_kategori",
	"m_runing",
	"m_berita",
	"m_menu",
	"m_video",
	"m_seting",
	"m_profil",
	"m_foto",
	"m_user","m_level","m_hak_akses"
);
