<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'template/depan';
$route['cpanel/login-cpnl'] = "login/log_admin";
$route['keluar'] = 'login/logout';

#dasah 
$route['cpanel/dashboard'] = "dashboard";


#berita 
$route['cpanel/berita'] = "berita";
$route['cpanel/berita/tambah'] = "berita/tambah";
$route['cpanel/berita/simpan'] = "berita/simpan";
$route['cpanel/berita/hapus'] = "berita/hapus";
$route['cpanel/berita/update-status'] = "berita/updatestatus";
$route['cpanel/berita/edit/(:any)'] = "berita/update/$1";
$route['cpanel/berita/simpanUpdate'] = "berita/simpanUpdate";
$route['cpanel/berita/adabacriini'] = "berita/cari_data";

#profil
$route['cpanel/profil'] = "profil";
$route['cpanel/profil/tambah'] = "profil/tambah";
$route['cpanel/profil/simpan'] = "profil/simpan";
$route['cpanel/profil/hapus'] = "profil/hapus";
$route['cpanel/profil/update-status'] = "profil/updatestatus";
$route['cpanel/profil/edit/(:any)'] = "profil/update/$1";
$route['cpanel/profil/simpanUpdate'] = "profil/simpanUpdate";
$route['cpanel/profil/adabacriini'] = "profil/cari_data";

#foto
$route['cpanel/foto'] = "foto";
$route['cpanel/foto/tambah'] = "foto/tambah";
$route['cpanel/foto/simpan'] = "foto/simpan";
$route['cpanel/foto/hapus'] = "foto/hapus";
$route['cpanel/foto/update-status'] = "foto/updatestatus";
$route['cpanel/foto/edit/(:any)'] = "foto/update/$1";
$route['cpanel/foto/simpanUpdate'] = "foto/simpanUpdate";
$route['cpanel/foto/adabacriini'] = "foto/cari_data";

$route['galeri-foto/detail/(:any)'] = 'mukajo/fotoDetail/$1';
$route['galeri-foto'] = 'mukajo/fotoFull';

#video
$route['cpanel/video'] = 'video';
$route['cpanel/video/tambah'] = 'video/tambah';
$route['cpanel/video/simpan'] = 'video/simpan';
$route['cpanel/video/edit/(:any)'] = "video/edit/$1";
$route['cpanel/video/simpanupdate'] = "video/simpanupdate";
$route['cpanel/video/tampila/(:any)'] = "video/tampil_a/$1";
$route['cpanel/video/delete/(:any)'] = "video/hapus/$1";


#route
$route['cpanel/kategori'] = "kategori";
$route['cpanel/kategori/simpan'] = "kategori/simpan";
$route['cpanel/kategori/vtambah'] = "kategori/vtambah";
$route['cpanel/kategori/hapus'] = "kategori/hapus";
$route['cpanel/kategori/vupdate'] = "kategori/vupdate";
$route['cpanel/kategori/update'] = "kategori/update";
$route['cpanel/kategori/update-status'] = "kategori/updatestatus";
$route['cpanel/kategori/adabacriini'] = "kategori/cari_data";

#runing
$route['cpanel/running'] = "runing";
$route['cpanel/running/simpan'] = "runing/simpan";
$route['cpanel/running/vtambah'] = "runing/vtambah";
$route['cpanel/running/hapus'] = "runing/hapus";
$route['cpanel/running/vupdate'] = "runing/vupdate";
$route['cpanel/running/update'] = "runing/update";
$route['cpanel/running/update-status'] = "runing/updatestatus";
$route['cpanel/running/adabacriini'] = "runing/cari_data";

$route['cpanel/menu'] = "menu";
$route['cpanel/menu/vtambah'] = "menu/vtambah";
$route['cpanel/menu/simpan'] = "menu/simpan";
$route['cpanel/menu/vupdate'] = "menu/vupdate";
$route['cpanel/menu/update'] = "menu/update";
$route['cpanel/menu/hapus'] = "menu/hapus";
$route['cpanel/menu/update-status'] = "menu/updatestatus";

#seting
$route['cpanel/seting/update'] = "seting/update_web";



#frond
$route['berita-kategori'] = "mukajo/berita_kategori";
$route['kategori-berita/(:any)/(:any)'] = "mukajo/kategori_berita/$1/$2";
$route['video-get-data'] = "mukajo/videoplay";



// content
$route['content/(:any)/(:any)'] ="mukajo/berita/$1/$2";
$route['cari-berita'] = "mukajo/cari_berita";



#user
$route['cpanel/user/cek-user-lamakwa'] =  'user/cek_user';
$route['cpanel/user/update-password'] = 'user/update_password';