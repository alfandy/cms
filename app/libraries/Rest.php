<?php

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class Rest {

    private $client;
    private $headers;
    private $headers_upload;
    private $ci;
    private $api_upload;
    private $api_uploadFile;
    private $headersupload;

    public function __construct() {
        $this->ci = & get_instance();
        $this->client = new Client(['base_uri' => SERVER_APP, 'timeout' => 2.0]);
        $this->headers = ['Authorization' => 'Bearer ' . $this->ci->session->userdata('token'), 'Content-Type' => 'application/json'];
        $this->api_upload = 'files.deezkon.com:8173/api/upload/foto-barang';
        // $this->api_upload = '192.168.1.69:8173/api/upload';

//        $this->api_upload = 'localhost:8173/api/upload/foto-barang';
        $this->headers_upload = ['Authorization' => 'Bearer ' . $this->ci->session->userdata('token')];

    }

    public function _get($api_name) {
        try {
            $response = $this->client->request('GET', $api_name, ['headers' => $this->headers]);
            return json_decode($response->getBody());
        } catch (\GuzzleHttp\Exception\RequestException $ex) {
            if (!empty($ex->getResponse())) {
                return $ex->getResponse()->getStatusCode();
            }
        }
    }

    public function _post($api_name, $data) {
        if ($this->cek_koneksi()) {
            try {
                $response = $this->client->post($api_name, [
                    'json' => $data,
                    'headers' => $this->headers
                ]);
                return TRUE;
            } catch (\GuzzleHttp\Exception\RequestException $ex) {
                return $ex->getResponse()->getStatusCode();
            }
        } else {
            return FALSE;
        }
    }

    public function _put($api_name, $data) {
        try {
            $response = $this->client->put($api_name, [
                'json' => $data,
                'headers' => $this->headers
            ]);
            return $response->getBody();
        } catch (\GuzzleHttp\Exception\RequestException $ex) {
            if (!empty($ex->getResponse())) {
                return $ex->getResponse()->getStatusCode();
            }
            return FALSE;
        }
    }

    public function _delete($api_name) {
        try {
            $response = $this->client->delete($api_name, [
                'headers' => $this->headers
            ]);
            return TRUE;
        } catch (\GuzzleHttp\Exception\RequestException $ex) {
            if (!empty($ex->getResponse())) {
                return $ex->getResponse()->getStatusCode();
            }
            return FALSE;
        }
    }

    protected function cek_koneksi() {
        $connected = @fsockopen(SERVER_MAIN, SERVER_PORT_APP);
        if ($connected) {
            $is_conn = true; //action when connected 

            fclose($connected);
        } else {
            $is_conn = false; //action in connection failure 
        }
        return $is_conn;
    }

   public function g_qrc($string , $width = '400' , $height = '400'){
       // https://chart.googleapis.com/chart?cht=qr&chl=Hello+world&chs=400x400        
       $clien_qrc = new Client();
       $response = $clien_qrc->request('GET', 'https://chart.googleapis.com/chart?cht=qr&chl='.$string.'&chs='.$width.'x'.$height);
        return $response->getBody();
       
   }

    public function _upload($data) {
        $client_upload = new Client();
        if ($this->cek_koneksi()) {
            try {
                $response = $client_upload->post($this->api_upload, [
                    'json' => $data,
                    'headers' => $this->headers
                ]);
                return json_decode($response->getBody())->urlfoto;
            } catch (\GuzzleHttp\Exception\RequestException $ex) {
                return $ex->getResponse()->getStatusCode();
            }
        } else {
            return FALSE;
        }
    }

    public function _uploadFile($data) {
        $client_upload = new Client();
        if ($this->cek_koneksi()) {
            try {

                $response = $client_upload->post($this->api_upload, [
                    'headers' => $this->headers_upload,
                    'multipart' => [
                        [
                            'name' => 'file',
                            'contents' => fopen($data,"r") //fopen('/home/abas/Pictures/barangdeezkon/tips-cara-membedakan-iphone-6-asli-dan-replika.png', 'r')
                        ]
                    ]

                ]);
                return json_decode($response->getBody())->urlfoto;
            } catch (\GuzzleHttp\Exception\RequestException $ex) {
                return $ex->getResponse()->getStatusCode();
            }
        } else {
            return FALSE;
        }
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

