<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class Auth {

    private $ci;
    private $client;
    private $jar;
    protected $accessToken;
    protected $refreshToken;

    function __construct() {
        $this->ci = & get_instance();
        $this->client = new GuzzleHttp\Client(['base_uri' => SERVER_LOGIN]);
        $this->jar = new \GuzzleHttp\Cookie\CookieJar;
    }

    public function login($username, $password) {
        if ($this->cek_koneksi()) {
            try {
                $request = $this->client->request('POST', 'oauth/token', [
                    'form_params' => [
                        'grant_type' => 'password',
                        'username' => $username,
                        'password' => $password,
                        'client_id' => CLIENT_ID,
                    ],
                    'headers' => [
                        'Authorization' => 'Basic ' . AUTHORIZTION,
                        'Content-Type' => 'application/x-www-form-urlencoded'
                    ],
                    'cookies' => $this->jar 
                ]);
                $response = json_decode($request->getBody());

                 // jwt akse   // 
                $this->accessToken = $response->access_token;
                $token = $this->accessToken;
                $base64Url = explode('.', $token)[1];
                $base164 = str_replace('-', '+', $base64Url);
                $base64  = str_replace('_', '/', $base164);
                $data_jwt =json_decode(base64_decode($base64)); 
                // print_r($data_jwt);
                // // die();
                // echo $data_jwt->user_name;
                // foreach ($data_jwt->authorities as  $value) {
                //     echo $value;
                // }


                
                $this->ci->session->set_userdata(array(
                    'token' => $token,
                    'akses' => $response->scope,
                    'user' => $data_jwt->user_name,
                    'akses' => $data_jwt->authorities,
                ));
                return '200';
            } catch (RequestException $ex) {
                if ($ex->getResponse()->getStatusCode() == '400') {
                    return '400';
                }
                return FALSE;
            }
        }else{
            return '408';
        }
        
    }

    public function logout() {
        $this->ci->session->set_userdata(array('token' => '', 'akses' => ''));
        session_destroy();
    }

    public  function sudah_login() {
        if (!empty($this->ci->session->userdata('token'))) {
            if ($this->ci->session->userdata('level') == '') {
                $level = $this->ci->rest->_get('cek-level-user')->levelUser;
                $this->ci->session->set_userdata('level' , $level );
            }
            return TRUE;            
        }
        return FALSE;
    }

   public function cekAkses()
    {

        // return $this->ci->rest->_get('cek-level-user')->levelUser;

    }

    public static function  cekAksesV2($nilai2 = NULL)
    {
        if ($nilai2 != NULL) {
            $akses = array(
                    1 => "ADMINISTRATOR",
                    2 => "TOKO",
                    3 => "PEMBELI",
                    4 => "CO",
                    5 => "ACCOUNTING",
                    6 => "CS",
                    7 => "FINANCIAL ASSISTANT",
                    8 => "VERIFIKATOR",
                ); 
            return $akses[$nilai2];
        }else{
            return '';
        }
        
    }

    protected function cek_koneksi() {
        $connected = @fsockopen(SERVER_MAIN, SERVER_PORT_AUTH);
        if ($connected) {
            $is_conn = true; //action when connected 

            fclose($connected);
        } else {
            $is_conn = false; //action in connection failure 
        }
        return $is_conn;
    }

}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

