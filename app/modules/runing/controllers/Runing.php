<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Runing extends MX_Controller {

    private $modul = '';
    private $redirect = '';

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('cpanel/login-cpnl','refresh');
        $this->modul = 'runing'; //nama modul
        $this->redirect = ''; //base url;
    }
    public function index() {
        $data['judul1'] = 'Running Text';
        $data['judul2'] = 'List Running Text';
        $data['list'] = $this->m_runing->get_all_s();
        $data['view'] = 'runing/list';
        $data['modul'] = $this->modul;
        echo Modules::run('template/belakan', $data);
    }
     public function cari_data(){
        $like = $this->input->get('data');
        $cari = $this->m_runing->cari_like($like , $limit = 10);
         $data['judul1'] = 'Berita';
        $data['list'] = $cari;
        $this->load->view('v_cari_runing',$data);
    }

    public function vtambah(){
        $this->load->view('tambah_runing');
    }

    public function simpan(){
       if ($this->input->post()) {
           $data = array(
                'id' => auto_inc('m_runing' , 'id'),
                'nama' => $this->input->post('nama'),
                'status' => '1',
                'hapus' => ''
           );
           $this->m_runing->insert($data);
       }else{
            redirect('','refresh');
       }
    }

    public function hapus(){
        if ($this->input->post()) {
            $id =  $this->input->post('data');
            $data = array(
                'hapus' => date_timestamp_get(date_create()),
            );
            $this->m_runing->update($id ,$data);
        }else{
            redirect('','refresh');
        }
    }

    

    public function vupdate(){
        if ($this->input->get('data')) {
            $id = $this->input->get('data');

            $data['lu'] = $this->m_runing->get_by(array('id' => $id));
            $this->load->view('update_runing' , $data);
        }else{
            redirect('','refresh');
        }
    }

    public function update(){
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data = array(
                "nama" => $this->input->post('nama')        
            );
            $this->m_runing->update($id , $data);
        }else{
            redirect('','refresh');
        }
    }

    public function updatestatus(){
        if ($this->input->post('data')) {
            $id = $this->input->post('data');
            $status = $this->input->post('status');
            $data = array(
                'status' => $status
            );

            $this->m_runing->update($id , $data);
        }else{
            redirect('','refresh');
        }
    }
}
