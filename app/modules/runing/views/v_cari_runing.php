<table class="table table-striped table-hover table-condensed table-bordered">
	<thead>
		<tr>
			<th style="width: 5%;">No</th>
			<th>Kategori</th>
			<th>Status</th>
			<th>#</th>
		</tr>
	</thead>
	<tbody>
		<?php $no=1;  if (!empty($list)): ?>
			<?php foreach ($list as $l): ?>
				<tr>
					<td><?php echo $no++ ?></td>
					<td><?php echo $l->nama ?></td>
					<td align="center">
						<?php 
							if ($l->status == '1'){
								$chek = "checked";
							}else{
								$chek = "";
							}
						 ?>
						<div class="form-group">
		                      <input  id="chek<?php echo $l->id?>" onchange="cekSatatus('<?php echo $l->id?>')" type="checkbox"  <?php echo $chek;?> />
		                    								                    
		                  </div>
					</td>
					<td align="center">
						<button type="button" class="btn btn-sm btn-warning" onclick="viewUpdate('<?php echo $l->id ?>')" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-pencil"></i></button>
						<button type="button" class="btn btn-sm btn-danger" onclick="haspusData('<?php echo $l->id; ?>');" ><i class="fa fa-trash"></i></button>
					</td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>