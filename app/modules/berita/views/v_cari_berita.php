<table class="table table-condensed table-hover table-striped table-bordered">
	<thead>
		<tr>
			<th>No</th>
			<th>Judul</th>
			<th>User</th>
			<th>Tanggal</th>
			<th>Kategori</th>
			<th>Baca</th>
			<th>Tampil</th>
			<!-- <th>Gambar</th> -->
			<th>#</th>
		</tr>
	</thead>
	<tbody>
		<?php $no=1; if (!empty($list)): ?>
			<?php foreach ($list as $l): ?>
				<tr>
					<td><?php echo $no++ ?></td>
					<td><?php echo $l->judul ?></td>
					<td><?php echo $l->iduser ?></td>
					<td><?php echo ts($l->ts) ?></td>
					<td><?php echo $l->kategori ?></td>
					<td><?php echo $l->count ?></td>
					<td align="center">
						<?php 
							if ($l->tampil == '1'){
								$chek = "checked";
							}else{
								$chek = "";
							}
						 ?>
						<div class="form-group">
		                      <input  id="chek<?php echo $l->id?>" onchange="cekSatatus('<?php echo $l->id?>')" type="checkbox"  <?php echo $chek;?> />
		                    								                    
		                  </div>
					</td>
					<!-- <td><?php //echo $l->gambar ?></td> -->
					<td>
						<a href="<?php echo site_url('cpanel/berita/edit/'.$l->id)?>" class="btn btn-sm btn-warning " data-toggle="tooltip" title="Edit <?php echo $judul1?>"><i class="fa fa-pencil"></i></a>
						<button type="button" class="btn btn-sm btn-danger" onclick="haspusData('<?php echo $l->id; ?>');" ><i class="fa fa-trash"></i></button>
					</td>
				</tr>
			<?php endforeach ?>
		<?php endif ?>
	</tbody>
</table>