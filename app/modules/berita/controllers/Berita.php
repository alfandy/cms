<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Berita extends MX_Controller {

    private $modul = '';
    private $redirect = '';
    private $userLog = '';

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('cpanel/login-cpnl','refresh');
        $this->modul = 'berita'; //nama modul
        $this->redirect = ''; //base url;
        $this->userLog = "admin";
    }
    public function index() {
        $data['list'] = $this->m_berita->get_all_r();
        $data['judul1'] = 'Berita';
        $data['judul2'] = 'List Berita';
        $data['view'] = 'berita/list';
        $data['modul'] = $this->modul;
        echo Modules::run('template/belakan', $data);
    }

    public function cari_data(){
        $like = $this->input->get('data');
        $cari = $this->m_berita->cari_like($like , $limit = 10);
         $data['judul1'] = 'Berita';
        $data['list'] = $cari;
        $this->load->view('v_cari_berita',$data);
    }

    public function tambah(){
        $data['kategori'] = drop_list('m_kategori' , 'id' , 'nama' , ' --Pilih Kategori--' );
        $data['judul1'] = 'Berita';
        $data['judul2'] = 'Tambah Berita';
        $data['view'] = 'berita/tambah';
        $data['modul'] = $this->modul;
        echo Modules::run('template/belakan', $data);
    }

    public function update($id){
        $cek = $this->m_berita->get_by(array('id' => $id , 'hapus' => '0'));
        if ($cek) {
            $data['kategori'] = drop_list('m_kategori' , 'id' , 'nama' , ' --Pilih Kategori--' );
            $data['l'] = $cek; 
            $data['judul1'] = 'Berita';
            $data['judul2'] = 'Update Berita';
            $data['view'] = 'berita/update';
            $data['modul'] = $this->modul;
            echo Modules::run('template/belakan', $data);
        }else{
            redirect('','refresh');
        }
    }

    public function simpan(){
        $extensi = pathinfo($_FILES['file']['name'] ,PATHINFO_EXTENSION );
        // echo $extensi;
        if ($extensi == 'png' || $extensi == 'jpg' || $extensi == 'jpeg' ||  $extensi == 'PNG' || $extensi == 'JPG' || $extensi == 'JPEG') {
            // berhasil upload sesuai extensi
              // print_r($this->input->post());
            $sourcePath = $_FILES['file']['tmp_name'];       // Storing source path of the file in a variable
            $dataUpload = $_FILES['file']['name'];
            $tes1 = str_replace(array('.png','.jpg','.jpeg' , '.JPG'), '.png', $dataUpload);
            $fix1 = date_timestamp_get(date_create()).'-'.$tes1;
            $targetPath = "./upload/".$fix1;// Target path where file is to be stored
            move_uploaded_file($sourcePath,$targetPath) ;    // Moving Uploaded file

            $data = array(
                'id' => auto_inc('m_berita' , 'id'),
                'judul' => $this->input->post('judul'),
                'isi' => $this->input->post('isi'),
                'iduser' => $this->userLog,
                'ts' => date_timestamp_get(date_create()),
                'tampil' => '1',
                'hapus' => '0',
                'idkategori' => $this->input->post('kategori'),
                'count' => '0',
                'caption' => $this->input->post('caption'),
                'gambar' => $fix1
            );

            $this->m_berita->insert($data);

            echo '1';
        }else{

            echo '0';
        }
    }

    public function simpanUpdate(){
        
        $id = $this->input->post('id');
        $dataBerita = $this->m_berita->get_by(array('id' => $id ));
        

        $extensi = pathinfo($_FILES['file']['name'] ,PATHINFO_EXTENSION );
        // echo $extensi;
        if ($extensi == 'png' || $extensi == 'jpg' || $extensi == 'jpeg' ||  $extensi == 'PNG' || $extensi == 'JPG' || $extensi == 'JPEG') {
             $pathgambar = realpath(APPPATH . '../upload/');
                if ($dataBerita->gambar != '') {
                    unlink($pathgambar . '/' . $dataBerita->gambar);
                }
            // berhasil upload sesuai extensi
              // print_r($this->input->post());
            $sourcePath = $_FILES['file']['tmp_name'];       // Storing source path of the file in a variable
            $dataUpload = $_FILES['file']['name'];
            $tes1 = str_replace(array('.png','.jpg','.jpeg' , '.JPG'), '.png', $dataUpload);
            $fix1 = date_timestamp_get(date_create()).'-'.$tes1;
            $targetPath = "./upload/".$fix1;// Target path where file is to be stored
            move_uploaded_file($sourcePath,$targetPath) ;    // Moving Uploaded file

            $gambarUpload = $fix1;

            
        }else{
            
            $gambarUpload =  $dataBerita->gambar;
        }
        $data = array(
            'judul' => $this->input->post('judul'),
            'isi' => $this->input->post('isi'),
            'iduser' => $this->userLog,
            'idkategori' => $this->input->post('kategori'),
            'caption' => $this->input->post('caption'),
            'gambar' => $gambarUpload
        );

        $this->m_berita->update($id , $data);

    }


    public function hapus()
    {
        if ($this->input->post('data')) {
            $id = $this->input->post('data');
            $data = array(
                'hapus' => date_timestamp_get(date_create())
            );
            $this->m_berita->update($id , $data);
        }else{
            redirect('','refresh');
        }
    }

    public function updatestatus(){
        if ($this->input->post('data')) {
            $id = $this->input->post('data');
            $status = $this->input->post('status');
            $data = array(
                'tampil' => $status
            );

            $this->m_berita->update($id , $data);
        }else{
            redirect('','refresh');
        }
    }
}
