<div class="container-fluid">
	<section class="content-header">
		<h4>Tambah Video</h4>
	</section>
	<div class="box box-primary">
		<section class="content">

			<?php echo form_open_multipart('cpanel/video/simpan',array('class'=>'form-horizontal')); ?>
			
			<div class="row form-group">
				<label class="control-label col-sm-2">File Vidio</label>
				<div class="col-sm-10">
				<span class="pull-right">Format : mp4 | flv</span>
					<input type="file" name="nama" value="" required="required" class="form-control">
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">durasi</label>
				<div class="col-sm-10">
					<?php echo form_input('durasi',set_value('durasi'),'class="form-control" required placeholder="durasi"'); ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Kategori</label>
				<div class="col-sm-10">
					<?php echo form_dropdown('kategori', $kategori, '' , 'class="form-control" required'); ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Keterangan</label>
				<div class="col-sm-10">
					<?php echo form_input('keterangan',set_value('keterangan'),'class="form-control" required placeholder="Keterangan"'); ?>
				</div>
			</div>

			
			
		<div class="row form-group">

			<div class="col-sm-12">
				<a href="<?= site_url('cpanel/video') ?>" class="btn btn-default pull-right">Kembali</a>
				<label class="col-sm-2"></label>
				<?php echo form_submit('submit','Simpan','class="btn btn-primary "'); ?>
			</div>
		</div>

		<?php echo  form_close(); ?>
	</section>
</div>
</div>


