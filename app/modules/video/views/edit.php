<!-- 
<link href="http://vjs.zencdn.net/c/video-js.css" rel="stylesheet">
<script src="http://vjs.zencdn.net/c/video.js"></script> -->
<link href="//vjs.zencdn.net/5.4.6/video-js.min.css" rel="stylesheet">
<script src="//vjs.zencdn.net/5.4.6/video.min.js"></script>
<div class="container-fluid">
	<section class="content-header">
		<h4>Edit Video</h4>
	</section>
	<div class="box box-primary">
		<section class="content">

			<?php echo form_open_multipart('cpanel/video/simpanupdate',array('class'=>'form-horizontal')); ?>
			<?= form_hidden('id', $e->id); ?>
			<?= form_hidden('ts', $e->ts); ?>
			<div class="row form-group">
				<label class="control-label col-sm-2"></label>
				<div class="col-sm-10">
					<video id="video1" class="video-js vjs-default-skin"  style="width: 100%;" height="240" 
                        data-setup='{"controls" : true, "autoplay" : false, "preload" : "auto"}'>
                        <source src="<?= base_url('upload/video/' .$e->id . '.flv') ?>" type="video/x-flv">
                    </video>
					
				</div>
			</div>
			
			<div class="row form-group">
				<label class="control-label col-sm-2">durasi</label>
				<div class="col-sm-10">
					<?php echo form_input('durasi',$e->durasi,'class="form-control" required placeholder="durasi"'); ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Keterangan</label>
				<div class="col-sm-10">
					<?php echo form_input('keterangan',$e->keterangan,'class="form-control" required placeholder="Keterangan"'); ?>
				</div>
			</div>
			<div class="row form-group">
				<label class="control-label col-sm-2">Kategori</label>
				<div class="col-sm-10">
					<?php echo form_dropdown('kategori', $kategori, $e->kategori , 'class="form-control" required'); ?>
				</div>
			</div>
			
			

			<div class="row form-group">

				<div class="col-sm-12">
					<a href="<?= site_url('adminweb/video.asp') ?>" class="btn btn-default pull-right">Kembali</a>
					<label class="col-sm-2"></label>
					<?php echo form_submit('submit','Simpan','class="btn btn-primary "'); ?>
				</div>
			</div>

			<?php echo  form_close(); ?>
		</section>
	</div>
</div>


