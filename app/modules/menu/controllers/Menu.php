<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Menu extends MX_Controller {

    private $modul = '';
    private $redirect = '';

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('cpanel/login-cpnl','refresh');
        $this->modul = 'menu'; //nama modul
        $this->redirect = ''; //base url;
    }
    public function index() {
        $data['judul1'] = 'Menu';
        $data['judul2'] = 'List Menu';
        $data['list'] = $this->m_menu->get_all_s();
        $data['view'] = 'menu/list';
        $data['modul'] = $this->modul;
        echo Modules::run('template/belakan', $data);
    }

    public function vtambah(){
        $this->load->view('tambah_menu');
    }

    public function simpan(){
       if ($this->input->post()) {
        $url = $this->input->post('url');
        $pecah = explode(base_url(), $url);
        if (!empty($pecah[1])) {
            $urlGet = 'punyaku/'.$pecah[1];
        }else{
            $urlGet = $url;
        }
           $data = array(
                'id' => auto_inc('m_menu' , 'id'),
                'nama' => $this->input->post('nama'),
                'url' => $urlGet,
                'status' => '1',
                'hapus' => ''
           );
           $this->m_menu->insert($data);
       }else{
            redirect('','refresh');
       }
    }

    public function hapus(){
        if ($this->input->post()) {
            $id =  $this->input->post('data');
            $data = array(
                'hapus' => date_timestamp_get(date_create()),
            );
            $this->m_menu->update($id ,$data);
        }else{
            redirect('','refresh');
        }
    }

    

    public function vupdate(){
        if ($this->input->get('data')) {
            $id = $this->input->get('data');

            $data['lu'] = $this->m_menu->get_by(array('id' => $id));
            $this->load->view('update_menu' , $data);
        }else{
            redirect('','refresh');
        }
    }

    public function update(){
        if ($this->input->post()) {
            $id = $this->input->post('id');
             $url = $this->input->post('url');
            $pecah = explode(base_url(), $url);
            if (!empty($pecah[1])) {
                $urlGet = 'punyaku/'.$pecah[1];
            }else{
                $urlGet = $url;
            }
            $data = array(
                "nama" => $this->input->post('nama'),
                "url" => $urlGet       
            );
            $this->m_menu->update($id , $data);
        }else{
            redirect('','refresh');
        }
    }

    public function updatestatus(){
        if ($this->input->post('data')) {
            $id = $this->input->post('data');
            $status = $this->input->post('status');
            $data = array(
                'status' => $status
            );

            $this->m_menu->update($id , $data);
        }else{
            redirect('','refresh');
        }
    }
}
