<section class="content-header">
  <h1>
    User
    <small>List user, menambah user, mengedit user</small>
  </h1>
</section>
<section class="content">
  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">List User</h3>
      <div class="box-tools pull-right">
        <a href="javascript::0" data-toggle="modal" data-target="#adduser" class="btn btn-primary">Tambah User</a>
      </div>
    </div>
    <div class="box-body">
        <table class="table table-striped" id="list_kategori">
          <thead>
            <tr>
              <td>No</td>
              <td>Username</td>
              <td>Nama Lengkap</td>
              <td>Level</td>
              <td>Status</td>
              <td>Aksi</td>
            </tr>
          </thead>
          <tbody>
          
            <?php 
              $no=1;
              foreach ($list as $row) {?>
            <tr>
              <td><?php echo $no++; ?></td>
              <td><?php echo $row->username; ?></td>
              <td><?= $row->display_name; ?></td>
              <td><?php echo $row->level_user; ?></td>
              <td>
                <?php
                  if ($row->status == '0'){ ?>
                    <a href="<?= site_url('adminweb/user/status/'.$row->id_user.'/1') ?>" class="btn btn-primary"><i class="fa fa-lock"></i></a>
                  <?php }else{ ?>
                    <a href="<?= site_url('adminweb/user/status/'.$row->id_user.'/0') ?>" class="btn btn-primary"><i class="fa fa-unlock"></i></a>
                  <?php }
                ?>
              </td>
              <td>
                <a href="javascript::0" data-toggle="modal" data-target="#edituser<?= $row->id_user ?>" class="btn btn-info"><i class="glyphicon glyphicon-pencil"></i></a>
                <a href="<?php echo site_url('adminweb/user/hapus/'.$row->id_user
                ); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-sm btn-warning"><i class="glyphicon glyphicon-trash"></i></a>
              </td>
            </tr>
            <!-- Modal -->
            <div class="modal fade" id="edituser<?= $row->id_user ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit User</h4>
                  </div>
                  <?= form_open('adminweb/user/edit.asp','class="form-horizontal"'); ?>
                  <div class="modal-body">
                    <div class="form-group">
                    <input type="hidden" name="id_user" value="<?php echo $row->id_user; ?>"></input>
                      <label class="col-md-3 control-label">Username</label>
                      <div class="col-md-9">
                        <?= form_input('username',$row->username,'class="form-control" placeholder="Username" required'); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Ganti Password</label>
                      <div class="col-md-9">
                        <?= form_password('password','','class="form-control" placeholder="Password" placeholder="Isi Password Untuk Mengganti"'); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Nama Lengkap</label>
                      <div class="col-md-9">
                        <?= form_input('display_name',$row->display_name,'class="form-control" placeholder="Nama Lengkap" required'); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                    <div class="form-group">
                      <label class="col-md-3 control-label">Level</label>
                      <div class="col-md-9">
                        <?= form_dropdown('level',$list_level,$row->level,'class="form-control" required'); ?>
                      </div>
                    </div>
                    <div class="clearfix"></div><br/>
                  </div>
                  <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                  </div>
                  <?= form_close(); ?>
                </div>
              </div>
            </div>
            <?php } ?>
            
          </tbody>
        </table>
        <?php echo form_close(); ?>
    </div><!-- /.box-body -->
  </div><!-- /.box -->

</section><!-- /.content -->


<!-- Modal -->
<div class="modal fade" id="adduser" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah User</h4>
      </div>
      <?= form_open('','class="form-horizontal"'); ?>
      <div class="modal-body">
        <div class="form-group">
          <label class="col-md-3 control-label">Username</label>
          <div class="col-md-9">
            <?= form_input('username','','class="form-control" placeholder="Username" required'); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Password</label>
          <div class="col-md-9">
            <?= form_password('password','','class="form-control" placeholder="Password" required'); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Nama Lengkap</label>
          <div class="col-md-9">
            <?= form_input('nama_lengkap','','class="form-control" placeholder="Nama Lengkap" required'); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Level</label>
          <div class="col-md-9">
            <?= form_dropdown('level',$list_level,'','class="form-control" required'); ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      <?= form_close(); ?>
    </div>
  </div>
</div>