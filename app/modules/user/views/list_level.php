<section class="content-header">
  <h1>
    Level User
    <small>List level user, menambah level user, mengedit level user</small>
  </h1>
</section>
<section class="content">
  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">List Level User</h3>
      <div class="box-tools pull-right">
        <a href="javascript::0" data-toggle="modal" data-target="#addlevel" class="btn btn-primary">Tambah Level</a>
      </div>
    </div>
    <div class="box-body">
        <table class="table table-striped" id="list_kategori">
          <thead>
            <tr>
              <td>No</td>
              <td>Level</td>
              <td>Keterangan</td>
              <td>Aksi</td>
            </tr>
          </thead>
          <tbody>
          
            <?php 
              $no=1;
              foreach ($list as $row) { ?>
            <tr>
              <td><?php echo $no++; ?></td>
              <td><?php echo $row->level_user; ?></td>
              <td><?php echo $row->keterangan; ?></td>
              <td>
                <?php 
                  if ($row->id_level_user === $this->session->userdata('level')){

                  }else{ ?>
                  <a href="<?php echo site_url('adminweb/level-user/hapus/'.$row->id_level_user
                ); ?>" onclick="return confirm('anda yakin untuk menghapus data ini');" class="btn btn-warning"><i class="fa fa-trash"></i></a>
                  <?php }
                ?>
              </td>
            </tr>
            <?php } ?>
            
          </tbody>
        </table>
        <?php echo form_close(); ?>
    </div><!-- /.box-body -->
  </div><!-- /.box -->

</section><!-- /.content -->


<!-- Modal -->
<div class="modal fade" id="addlevel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Tambah Level</h4>
      </div>
      <?= form_open('','class="form-horizontal"'); ?>
      <div class="modal-body">
        <div class="form-group">
          <label class="col-md-3 control-label">Level</label>
          <div class="col-md-9">
            <?= form_input('level','','class="form-control" placeholder="Nama Level" required'); ?>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">Keterangan</label>
          <div class="col-md-9">
            <?= form_input('keterangan','','class="form-control" placeholder="Keterangan" required'); ?>
          </div>
        </div>
         <div class="form-group">
          <label class="col-md-3 control-label">Hak Akses Menu</label>
          <div class="col-md-9">
            <?= form_multiselect('hak_akses[]',$list_menu,'','class="form-control"'); ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="reset" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary">Simpan</button>
      </div>
      <?= form_close(); ?>
    </div>
  </div>
</div>
