<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Kategori extends MX_Controller {

    private $modul = '';
    private $redirect = '';

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('cpanel/login-cpnl','refresh');
        $this->modul = 'kategori'; //nama modul
        $this->redirect = ''; //base url;
    }
    public function index() {
        $data['judul1'] = 'Kategori';
        $data['judul2'] = 'List Kategori';
        $data['list'] = $this->m_kategori->get_all_s();
        $data['view'] = 'kategori/list';
        $data['modul'] = $this->modul;
        echo Modules::run('template/belakan', $data);
    }

     public function cari_data(){
        $like = $this->input->get('data');
        $cari = $this->m_kategori->cari_like($like , $limit = 10);
         $data['judul1'] = 'Berita';
        $data['list'] = $cari;
        $this->load->view('v_cari_kategori',$data);
    }

    public function vtambah(){
        $this->load->view('tambah_kategori');
    }

    public function simpan(){
       if ($this->input->post()) {
           $data = array(
                'id' => auto_inc('m_kategori' , 'id'),
                'nama' => $this->input->post('kategori'),
                'status' => '1',
                'hapus' => ''
           );
           $this->m_kategori->insert($data);
       }else{
            redirect('','refresh');
       }
    }

    public function hapus(){
        if ($this->input->post()) {
            $id =  $this->input->post('data');
            $data = array(
                'hapus' => date_timestamp_get(date_create()),
            );
            $this->m_kategori->update($id ,$data);
        }else{
            redirect('','refresh');
        }
    }

    

    public function vupdate(){
        if ($this->input->get('data')) {
            $id = $this->input->get('data');

            $data['lu'] = $this->m_kategori->get_by(array('id' => $id));
            $this->load->view('update_kategori' , $data);
        }else{
            redirect('','refresh');
        }
    }

    public function update(){
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data = array(
                "nama" => $this->input->post('kategori')        
            );
            $this->m_kategori->update($id , $data);
        }else{
            redirect('','refresh');
        }
    }

    public function updatestatus(){
        if ($this->input->post('data')) {
            $id = $this->input->post('data');
            $status = $this->input->post('status');
            $data = array(
                'status' => $status
            );

            $this->m_kategori->update($id , $data);
        }else{
            redirect('','refresh');
        }
    }
}
