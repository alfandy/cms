<div class="widget kopa-article-list-7-widget clearfix">

	<div class="widget-title widget-title-style-6 clearfix">
		<!-- <span class="rectangle"></span> -->
		<h4 class="pull-left"><?php echo $judulss?></h4>
		<a href="#" class="load-more pull-left"><?php echo $kategori?></a>
	</div>
	<?php if (!empty($lcontenAll)): ?>
		<?php foreach ($lcontenAll as $b_k ): ?>
			<div class="col-md-12">
				<div class="blog-item rounded shadow">
					<div class="media">
						<a class="pull-left" href="<?php echo site_url('galeri-foto/detail/'.$b_k->id)?>">
							<img class="media-object thumbnail img-responsive" style="width: 100px; " src="<?php echo base_url().'upload/foto/'.$b_k->gambar?>" alt="...">

						</a>

						<div class="media-body">
							<a href="<?php echo site_url('galeri-foto/detail/'.$b_k->id)?>">
								<h4 class="media-heading"><?php echo $b_k->judul?></h4>
							</a>
							<small><?php  echo ts($b_k->ts) ?></small>
						</div>
					</div><!-- media -->


				</div>
			</div>
		<?php endforeach ?>
	<?php else: ?> 
		<h3> Belum Ada Foto</h3>
	<?php endif ?>   
</div>	