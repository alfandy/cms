<?php if (!empty($berita_kategori)): ?>
	<ul class="clearfix">
		<?php foreach ($berita_kategori as $b_k): ?>
			<li>
			    <article class="entry-item">
			        <div class="entry-thumb">
			            <a href="<?php echo site_url('content/'.$b_k->id.'/'.flag($b_k->judul))?>"><img src="<?php echo base_url().'upload/'.$b_k->gambar?>" alt="" /></a>
			            <a href="<?php echo site_url('content/'.$b_k->id.'/'.flag($b_k->judul))?>" class="entry-categories clearfix"><i class=""></i><span class="pull-left"><?php echo $b_k->kategori?></span></a>
			            <!-- <a class="entry-comments" href="#">209</a> -->
			        </div>
			        <div class="entry-content">
			            <div class="meta-box">
			               <!--  <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Price Peterson</a></span> -->
			                <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left"><?php echo ts($b_k->ts)?></span></span>
			            </div>
			            <h6 class="entry-title"><a href="<?php echo site_url('content/'.$b_k->id.'/'.flag($b_k->judul))?>"><?php echo $b_k->judul?></a></h6>
			        </div>
			    </article>    
			</li>
		<?php endforeach ?>
	</ul>
	<?php if ($idKategori != '0'): ?>
		<a href="<?php echo site_url('kategori-berita/'.$idKategori.'/'.flag($kategori->nama))?>" class="btn btn-sm btn-primary pull-right" style="margin-top: 20px;margin-bottom: 20px;">	Selengkapnya</a>
	<?php endif ?>
<?php else: ?>
    <article class="entry-item" style="margin-top: 20px;">
        <center><h3>Belum Ada Berita</h3></center>
    </article>    
	
<?php endif ?>

