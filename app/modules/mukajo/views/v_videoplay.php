<link href="//vjs.zencdn.net/5.4.6/video-js.min.css" rel="stylesheet">
<script src="//vjs.zencdn.net/5.4.6/video.min.js"></script>

<?php if (!empty($videoT1)): ?>
    <div class="entry-thumb">
        <video id="video1" class="video-js vjs-default-skin"  style="width: 100%;" height="240" 
            data-setup='{"controls" : true, "autoplay" : false, "preload" : "auto"}'>
            <source src="<?= base_url('upload/video/' .$videoT1->file) ?>" >
        </video>
    </div>
    <div class="entry-content clearfix">
        <h6 class="entry-title"><a href="#"><?php echo $videoT1->keterangan?></a></h6>
        
    </div>
<?php endif ?>