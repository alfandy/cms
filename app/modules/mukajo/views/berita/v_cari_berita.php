<div class="widget kopa-article-list-7-widget clearfix">

	<div class="widget-title widget-title-style-6 clearfix">
		<!-- <span class="rectangle"></span> -->
		<h4 class="pull-left"><?php echo $judulss?></h4>
		<a href="#" class="load-more pull-left"><?php echo $kategori?></a>
	</div>
	<div class="widget kopa-newsletter-widget">
        
        <?php echo form_open('cari-berita', 'class="newsletter-form "');?>
            <p class="input-email " >
                <input type="text" size="70" class="email" value="<?php echo $cariapa?>" name="s" style="width: 85%;" >
                <button type="submit" class="submit"><i class="fa fa-search"></i>Cari</button>
            </p>                    
        <?php echo form_close(); ?>
    </div>
	<?php if (!empty($berita_kategori)): ?>
		<?php foreach ($berita_kategori as $b_k ): ?>
			<div class="col-md-12">
				<div class="blog-item rounded shadow">
					<div class="media">
						<a class="pull-left" href="<?php echo site_url('content/'.$b_k->id.'/'.flag($b_k->judul))?>">
							<img class="media-object thumbnail img-responsive" style="width: 100px; " src="<?php echo base_url().'upload/'.$b_k->gambar?>" alt="...">

						</a>

						<div class="media-body">
							<a href="<?php echo site_url('content/'.$b_k->id.'/'.flag($b_k->judul))?>">
								<h4 class="media-heading"><?php echo $b_k->judul?></h4>
							</a>
							<small><?php  echo ts($b_k->ts) ?></small>
						</div>
					</div><!-- media -->


				</div>
			</div>
		<?php endforeach ?>
	<?php else: ?> 
		<h3> Belum Ada Berita</h3>
	<?php endif ?>   
</div>	