<div class="widget kopa-article-list-7-widget clearfix">

	<div class="widget-title widget-title-style-6 clearfix">
		<!-- <span class="rectangle"></span> -->
		<h4 class="pull-left"><?php echo $judulss?></h4>
		<a href="#" class="load-more pull-left"><?php echo $kategori?></a>
	</div>
	<?php if (!empty($lconten)): ?>
		<div class="col-md-12">
			<h1 class="entry-title clearfix"><?php echo $lconten->judul ?></h1>
			<div class="ktz-inner-metasingle">

				<span class="entry-date updated">
					<a href="#" title="11:59" rel="bookmark">
						<time datetime="" pubdate=""><?php echo ts($lconten->ts)?></time>
					</a>
				</span>
				<?php if ($lconten->count != '0'): ?>
					<span class="entry-view">
						<?php echo $lconten->count?>x dibaca 
					</span>               
				<?php endif ?>                
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="blog-img">
						<!-- 750x500 -->
						<img style="width: 100%;" src="<?php echo base_url().'upload/'.$lconten->gambar?>" class="img-responsive" >
					</div>
					<p style="text-align: justify;">
						<div style="font-size: 15px;">
							<?php echo $lconten->isi?>
						</div>
					</p>

					<div class="col-sm-12">
						<?= sharethis('facebook' , curPageURL()); ?>
	                    <?= sharethis('twitter' , curPageURL()); ?>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="fb-comments" data-href="<?php echo curPageURL(); ?>" data-numposts="5"></div>
				</div>
				
			</div>







		</div>
	<?php else: ?> 
		<h3> Belum Ada Berita</h3>
	<?php endif ?>   
</div>	