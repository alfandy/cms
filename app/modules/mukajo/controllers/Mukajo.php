<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Mukajo extends MX_Controller {

    private $modul = '';
    private $redirect = '';

    public function __construct() {
        parent::__construct();
        $this->modul = 'mukajo'; //nama modul
        $this->redirect = ''; //base url;
    }

    public function fotoDetail($idFoto){
      $cek = $this->m_foto->get_by(array('id' => $idFoto , 'hapus' => '0' , 'tampil' => '1'));
      if ($cek) {
          $data['kategori'] = 'Foto';
          $bacaDulu = array(
              'count' => $cek->count + 1
            );
          $this->m_foto->update($idFoto , $bacaDulu);
          $data['fotoLainya'] = $this->m_foto->fotoLainya($idFoto);
          $data['lconten'] =  $cek;
          $data['judulss'] = "MultimediaNews";
          $data['view'] = 'foto/v_foto';
          $data['modul'] = $this->modul;
          echo Modules::run('template/depan', $data);
      }else{
        redirect('','refresh');
      }
    }

    public function fotoFull(){
          $data['kategori'] = 'Foto';
          $cek = $this->m_foto->getAllLimit();
          // $data['fotoLainya'] = $this->m_foto->fotoLainya($idFoto);
          $data['lcontenAll'] =  $cek;
          $data['judulss'] = "MultimediaNews";
          $data['view'] = 'foto/v_foto_all';
          $data['modul'] = $this->modul;
          echo Modules::run('template/depan', $data);
    }

    public function kategori_berita($id ,$kategori){
        $cek = $this->m_kategori->get_by(array('id' => $id , 'status' => '1' , 'hapus' => '0' ));
        if ($cek) {
          $data['kategori'] = $cek->nama;
          $data['berita_kategori'] =  $this->m_berita->get_berita_kategori($id ,$start = 0 , $limit = 10 , $return = 'result');
          $data['judulss'] = "MultimediaNews";
          $data['view'] = 'kategori_berita/v_kategori_berita';
          $data['modul'] = $this->modul;
          echo Modules::run('template/depan', $data);
        }else{
          redirect('','refresh');
        }
    }

    public function berita($id , $kategori){
      $cek = $this->m_berita->get_by(array('id' => $id , 'tampil' => '1' , 'hapus' => '0' ));
        if ($cek) {
          $bacaDulu = array(
              'count' => $cek->count + 1
            );
          $this->m_berita->update($id , $bacaDulu);
          $cek2 = $this->m_kategori->get_by(array('id' => $cek->idkategori));
          $data['kategori'] = $cek2->nama;
          $data['lconten'] =  $cek;
          $data['judulss'] = "MultimediaNews";
          $data['view'] = 'berita/v_berita';
          $data['modul'] = $this->modul;
          echo Modules::run('template/depan', $data);
        }else{
          redirect('','refresh');
        }
    }

    public function cari_berita(){
      if ($this->input->post('s') !='' ) {
            $like = $this->input->post('s');
            // $cek = $this->m_kategori->get_by(array('id' => $id , 'status' => '1' , 'hapus' => '0' ));
            
              $data['kategori'] = "Pencarian";
              $data['cariapa'] = $like;
              $data['berita_kategori'] =  $this->m_berita->get_berita_like($like ,$start = 0 , $limit = 10 , $return = 'result');
              $data['judulss'] = "MultimediaNews";
              $data['view'] = 'berita/v_cari_berita';
              $data['modul'] = $this->modul;
              echo Modules::run('template/depan', $data);
            
      }else{
        redirect('','refresh');
      }
      
    }

    public function berita_kategori() {
       if ($this->input->get()) {
           $id_kategori = $this->input->get('data');
           if ($id_kategori == NULL) {
              $data['idKategori'] = '0';
              $data['berita_kategori'] = $this->m_berita->get_aktif($start = 4 , $limit = 2 , $return = 'result');
           }else{
              $data['idKategori'] = $id_kategori;
              $data['kategori'] = $this->m_kategori->get_by(array('id' => $id_kategori));
              $data['berita_kategori'] =  $this->m_berita->get_berita_kategori($id_kategori ,$start = 0 , $limit = 2 , $return = 'result');
           }
           $this->load->view('v_berita_kategori', $data);
       }
    }


    public function videoplay(){
      $id = $this->input->get('data');
      $cek = $this->m_video->get_by(array('id' => $id));
      $data['videoT1'] = $cek;
      
      $this->load->view('v_videoplay', $data);
    }

    

}
