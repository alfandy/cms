<!-- Content Header (Page header) -->
<section class="content-header">
	<h1>
		<?php echo $judul1?>
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">Dashboard</li>
	</ol>
</section>

<!-- Main content -->
<section class="content">

	<div class="row">
		<!-- Left col -->
		<section class="col-lg-12 connectedSortable">
			<!-- TO DO List -->
			<div class="box box-primary">
				<div class="box-header">
					<i class="ion ion-clipboard"></i>
					<h3 class="box-title"><?php echo $judul2;?></h3>

					<a href="<?php echo site_url('cpanel/profil/tambah')?>" class="btn btn-sm btn-primary pull-right" data-toggle="tooltip" title="Tambah <?php echo $judul1?>"><i class="fa fa-plus"></i> <?php echo $judul1;?></a>

				</div><!-- /.box-header -->
				<div class="box-body">
					<div style="padding: 10px;">
		                <div class="input-group" style="width: 150px;">
		                  <input type="text" onkeyup="cariDataLe(this);" name="table_search" class="form-control input-sm pull-right" placeholder="Search">
		                  <div class="input-group-btn">
		                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
			               </div>
			            </div>
			        </div>
					<div class="table-responsive">
						<div id="tempelDataBro">
							
							<table class="table table-condensed table-hover table-striped table-bordered">
								<thead>
									<tr>
										<th>No</th>
										<th>Nama</th>
										<th>Jabatan</th>
										<th>User</th>
										<th>Tanggal</th>
										<th>Tampil</th>
										<th>#</th>
									</tr>
								</thead>
								<tbody>
									<?php $no=1; if (!empty($list)): ?>
										<?php foreach ($list as $l): ?>
											<tr>
												<td><?php echo $no++ ?></td>
												<td><?php echo $l->judul ?></td>
												<td><?php echo $l->caption ?></td>
												<td><?php echo $l->iduser ?></td>
												<td><?php echo ts($l->ts) ?></td>
												<td align="center">
													<?php 
														if ($l->tampil == '1'){
															$chek = "checked";
														}else{
															$chek = "";
														}
													 ?>
													<div class="form-group">
									                      <input  id="chek<?php echo $l->id?>" onchange="cekSatatus('<?php echo $l->id?>')" type="checkbox"  <?php echo $chek;?> />
									                    								                    
									                  </div>
												</td>
												<!-- <td><?php //echo $l->gambar ?></td> -->
												<td>
													<a href="<?php echo site_url('cpanel/profil/edit/'.$l->id)?>" class="btn btn-sm btn-warning " data-toggle="tooltip" title="Edit <?php echo $judul1?>"><i class="fa fa-pencil"></i></a>
													<button type="button" class="btn btn-sm btn-danger" onclick="haspusData('<?php echo $l->id; ?>');" ><i class="fa fa-trash"></i></button>
												</td>
											</tr>
										<?php endforeach ?>
									<?php endif ?>
								</tbody>
							</table>
						</div>
					</div>
				</div><!-- /.box-body -->
				<div class="box-footer clearfix no-border">

				</div>
			</div><!-- /.box -->



		</section><!-- /.Left col -->
		<!-- right col (We are only adding the ID to make the widgets sortable)-->

	</div><!-- /.row (main row) -->

</section><!-- /.content -->


<script>
	function cariDataLe(dta){
		var data = dta.value;
		$.ajax({
            url: '<?php echo site_url('cpanel/profil/adabacriini')?>',
            type: 'GET',
            dataType: 'html',
            data: {data: data},
            beforeSend : function(){
                var html = '<div class="overlay">'+
                '<i class="fa fa-refresh fa-spin"></i>'+
                '</div>'
                $("#tempelDataBro").html(html);
            },
            success : function(get){
                $("#tempelDataBro").html(get);
            },
            error : function(){

            }
        });
		
	}

	function cekSatatus(datai){
         // console.log(datai);
        var cek = document.getElementById("chek"+datai);
        if (cek.checked) {
            var chek = '1';
        }else{
        	var chek = '0';
        }

        $.ajax({
            url: '<?php echo site_url('cpanel/profil/update-status') ?>',
            type: 'POST',
            dataType: 'html',
            data: {data: datai , status : chek },
            beforeSend : function(){

            },
            success : function(data){
                
                $.toaster({priority: 'success', title: 'Success', message: 'Berhasil Update Data'});
            },
            error : function (){
                $.toaster({priority: 'error', title: 'Error', message: 'Gagal Update Data'});
            }
        });


    }
	function haspusData(datai){
        var datai = datai;

        var reslut = confirm('Anda Yakin Untuk Menghapus Daata Ini');
        if (reslut) {
            
            $.ajax({
                url: '<?php echo site_url('cpanel/profil/hapus') ?>',
                type: 'POST',
                dataType: 'html',
                data: {data: datai},
                beforeSend : function(){

                },
                success : function(data){
                    
                    $.toaster({priority: 'success', title: 'Success', message: 'Berhasil Hapus Data'});
                    window.location = "<?php echo site_url('cpanel/profil') ?>";
                },
                error : function (){
                    $.toaster({priority: 'error', title: 'Error', message: 'Gagal Hapus Data'});
                }
            });
        }
        
    }
</script>
