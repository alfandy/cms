<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Dashboard extends MX_Controller {

    private $modul = '';
    private $redirect = '';

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('cpanel/login-cpnl','refresh');
        $this->modul = 'dashboard'; //nama modul
        $this->redirect = ''; //base url;
    }

    public function index() {
        $data['titel'] = 'Dasboard';
        $data['view'] = 'admin/v_index';
        $data['modul'] = $this->modul;
        echo Modules::run('template/belakan', $data);
    }

    

}
