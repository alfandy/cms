<?php 

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Template extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();

	}

	public function index($data = NULL){
		// if (!empty($data)) {
		// 	$this->load->view('v1/v_index' , $data); 
		// 	# code...
		// }else{
		// 	$this->load->view('v1/v_index' , $data);
		// }

	}

	public function belakan($data = NULL){
		if (!empty($data)) {
			$data['setingWeb'] = $this->m_seting->get_by(array('id' => '1'));
			$this->load->load->view('v3/v_index' , $data);
		}else{
			$data['setingWeb'] = $this->m_seting->get_by(array('id' => '1'));
			$this->load->load->view('v3/v_index', $data);
		}
	}

	public function depan($data = NULL){
		if (!empty($data)) {
			$data['warna'] = "#d72120;";
			$data['menuBar'] = $this->m_menu->get_aktif($start = 0 , $limit = 7 , $return = 'result');
			$data['setingWeb'] = $this->m_seting->get_by(array('id' => '1'));
			$data['teks_berjalan'] = $this->m_runing->get_aktif(5);

			$data['list_kategori'] = $this->m_kategori->get_aktif();

          	$data['berita_t'] = $this->m_berita->get_aktif($start = 0 , $limit = 5 , $return = 'result');

			$this->load->view('v2/v_index', $data);
		}else{
			$data['fotoTampil'] = $this->m_foto->get_aktif($start = 0, $limit = 1 , $return = 'row');
			$data['warna'] = "#d72120;";
			$data['silderFoto'] = $this->m_profil->get_aktif($start = 0 , $limit = 5 , $return = 'result');
			$data['berita111kanan'] = $this->m_berita->get_by(array('hapus'=> '0' , 'tampil' => '1'));
			$data['videoT1'] = $this->m_video->get_all_tampil_limit(1 , "row");
			$data['videoT'] = $this->m_video->get_all_tampil_limit(5);
			$data['menuBar'] = $this->m_menu->get_aktif($start = 0 , $limit = 7 , $return = 'result');
			$data['setingWeb'] = $this->m_seting->get_by(array('id' => '1'));

			$data['kategori_berita'] = drop_list('m_kategori' , 'id' , 'nama' ,'--Kategori Berita --', 'get_aktif' );
			$data['berita_kategori_awal'] = $this->m_berita->get_aktif($start = 4 , $limit = 2 , $return = 'result');
			$sliderrrr = '1';
			$data['sliderrrr'] =$sliderrrr;
			if ($sliderrrr == '0') {
				$data['berita_3'] = $this->m_berita->get_aktif($start = 1 , $limit = 3 , $return = 'result');
			}else{

				$data['berita_3'] = $this->m_berita->get_aktif($start = 0 , $limit = 3 , $return = 'result');
			}
			$data['berita_1'] = $this->m_berita->get_aktif($start = 0 , $limit = 1 , $return = 'row');
			$data['teks_berjalan'] = $this->m_runing->get_aktif(5);

			$this->load->view('v2/v_index', $data);
		}
	}
}

