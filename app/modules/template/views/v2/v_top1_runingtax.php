<div class="kopa-header-top">

    <div class="wrapper clearfix">

        <div class="row">

            <div class="col-md-10 col-sm-6">

                <div class="kp-headline-wrapper clearfix">
                    <span class="kp-headline-title" style="background-color: <?php echo $warna?>;">Breaking news</span>
                    <div class="kp-headline clearfix" style="margin-left:200px;">                        
                        <dl class="ticker-1 clearfix">'
                            <?php if (!empty($teks_berjalan)): ?>
                                <?php foreach ($teks_berjalan as $t_b): ?>
                                    <dd><a href="#"><?php echo $t_b->nama?></a></dd>
                                <?php endforeach ?>
                            <?php endif ?>
                        </dl>
                        <!--ticker-1-->
                    </div>
                    <!--kp-headline-->
                </div>
                <!-- kp-headline-wrapper -->

            </div>
            <!-- col-md-7 -->

            <div class="col-md-2 col-sm-6">

                <div class="search-nav-box clearfix">

                    <div class="search-box pull-right clearfix">
                        <?php echo form_open('cari-berita', 'class="search-form pull-left clearfix"');?>
                            <input required="required" type="text" onBlur="if (this.value == '')
                            this.value = this.defaultValue;" onFocus="if (this.value == this.defaultValue)
                            this.value = '';" value="Search" name="s" class="search-text">
                            <button type="submit" class="search-submit"><i class="fa fa-search"></i>
                            </button>
                        <?php echo form_close(); ?>
                        <!-- search-form -->
                    </div>
                    <!--search-box-->

                    
                    <!-- navbar-nav -->
                    
                </div>
                <!-- search-nav-box -->

            </div>
            <!-- col-md-5 -->

        </div>
        <!-- row -->

    </div>
    <!-- wrapper -->

</div>