<div class="kopa-home-slider-box loading">
        
    <div class="owl-carousel kopa-home-slider-1">

        <div class="item">
            <article class="entry-item video-post">
                <div class="entry-thumb">
                    <img src="<?php echo base_url()?>depan2/placeholders/post-image/post-1.jpg" alt="" />
                    <div class="mask"><a href="#"></a></div>
                </div>
                <!-- entry-thumb -->
                <div class="entry-content">
                    <a href="#" class="entry-categories clearfix"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                    <h2 class="entry-title"><a href="#">Enjoy High Desert Design</a></h2>
                    <div class="meta-box">
                        <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept & art direction</a></span>
                        <span class="entry-date">July 8, 2014</span>
                    </div>
                    <!-- meta-box -->
                    <p>Situated just one block from the beach in Venice, California, this award-winning local landmark epitomizes modernist cool.</p>
                </div>
                <!-- entry-content -->
                <div class="bg-bottom"></div>
            </article>
            <!-- entry-item -->
        </div>
        <!-- item -->

        <div class="item">
            <article class="entry-item standard-post">
                <div class="entry-thumb">
                    <img src="<?php echo base_url()?>depan2/placeholders/post-image/post-1.jpg" alt="" />
                    <div class="mask"><a href="#"></a></div>
                </div>
                <!-- entry-thumb -->
                <div class="entry-content">
                    <a href="#" class="entry-categories clearfix"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                    <h2 class="entry-title"><a href="#">Enjoy High Desert Design</a></h2>
                    <div class="meta-box">
                        <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept & art direction</a></span>
                        <span class="entry-date">July 8, 2014</span>
                    </div>
                    <!-- meta-box -->
                    <p>Situated just one block from the beach in Venice, California, this award-winning local landmark epitomizes modernist cool.</p>
                </div>
                <!-- entry-content -->
                <div class="bg-bottom"></div>
            </article>
            <!-- entry-item -->
        </div>
        <!-- item -->

        <div class="item">
            <article class="entry-item gallery-post">
                <div class="entry-thumb">
                    <img src="<?php echo base_url()?>depan2/placeholders/post-image/post-1.jpg" alt="" />
                    <div class="mask"><a href="#"></a></div>
                </div>
                <!-- entry-thumb -->
                <div class="entry-content">
                    <a href="#" class="entry-categories clearfix"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                    <h2 class="entry-title"><a href="#">Enjoy High Desert Design</a></h2>
                    <div class="meta-box">
                        <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept & art direction</a></span>
                        <span class="entry-date">July 8, 2014</span>
                    </div>
                    <!-- meta-box -->
                    <p>Situated just one block from the beach in Venice, California, this award-winning local landmark epitomizes modernist cool.</p>
                </div>
                <!-- entry-content -->
                <div class="bg-bottom"></div>
            </article>
            <!-- entry-item -->
        </div>
        <!-- item -->

        <div class="item">
            <article class="entry-item audio-post">
                <div class="entry-thumb">
                    <img src="<?php echo base_url()?>depan2/placeholders/post-image/post-1.jpg" alt="" />
                    <div class="mask"><a href="#"></a></div>
                </div>
                <!-- entry-thumb -->
                <div class="entry-content">
                    <a href="#" class="entry-categories clearfix"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                    <h2 class="entry-title"><a href="#">Enjoy High Desert Design</a></h2>
                    <div class="meta-box">
                        <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept & art direction</a></span>
                        <span class="entry-date">July 8, 2014</span>
                    </div>
                    <!-- meta-box -->
                    <p>Situated just one block from the beach in Venice, California, this award-winning local landmark epitomizes modernist cool.</p>
                </div>
                <!-- entry-content -->
                <div class="bg-bottom"></div>
            </article>
            <!-- entry-item -->
        </div>
        <!-- item -->
        
    </div>
    <!-- kopa-home-slider -->

</div>