<div class="widget kopa-article-list-5-widget">

    <h2 class="widget-title widget-title-style-4">
        
        <span class="text-after">Foto</span>
    </h2>
    <!-- widget-title -->

    <ul class="clearfix">
        
            <?php if (!empty($fotoTampil)): ?>
                <li>
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="<?php echo site_url('galeri-foto/detail/'.$fotoTampil->id)?>"><img alt="" src="<?php echo base_url('upload/foto/'.$fotoTampil->gambar)?>"></a>
                                <!-- <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a> -->
                                <!-- <a href="#" class="entry-comments">104</a> -->
                            </div>
                            <div class="entry-content">
                                <div class="meta-box">
                                    
                                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left"><?php echo ts($fotoTampil->ts)?></span></span>
                                </div>
                                <h6 class="entry-title"><a href="<?php echo site_url('galeri-foto/detail/'.$fotoTampil->id)?>"><?php echo $fotoTampil->judul?></a></h6>
                            </div>
                        </article>    
                </li>
                <a href="<?php echo site_url('galeri-foto') ?>" class="btn btn-sm btn-primary pull-right" >Selengkapnya</a>
            <?php endif ?>

    </ul>

    <span class="widget-border-top"></span>
    
</div>