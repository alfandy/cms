<div class="widget-area-2 pull-left">

    <div class="widget kopa-article-list-7-widget clearfix">
        
        <div class="widget-title widget-title-style-6 clearfix">
            <span class="rectangle"></span>
            <h4 class="pull-left">Quick reads</h4>
            <a href="#" class="load-more pull-left">Event more news</a>
        </div>
        <!-- widget-title -->

        <article class="last-item pull-right entry-item standard-post">
            <div class="entry-thumb">
                <img src="<?php echo base_url()?>depan2/placeholders/post-image/post-21.jpg" alt="" />
                <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Fashion</span></a>
                <a href="#" class="entry-comments">17</a>
                <a class="entry-icon" href="#"><span><i></i></span></a>
            </div>
            <h6 class="entry-title"><a href="#">How the Harambe Entrepreneur Alliance Is Providing a Boost for African Start-Ups</a></h6>
        </article>
        <!-- last-item -->

        <ul class="older-post pull-left">
            <li>
                <article class="entry-item clearfix">
                    <div class="entry-thumb pull-left">
                        <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-22.jpg" alt="" /></a>
                    </div>
                    <div class="entry-content">
                        <h6 class="entry-title"><a href="#">3 Misconceptions Christians Have About Marrying Non-Christians</a></h6>
                    </div>
                </article>
            </li>
            <li>
                <article class="entry-item clearfix">
                    <div class="entry-thumb pull-left">
                        <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-23.jpg" alt="" /></a>
                    </div>
                    <div class="entry-content">
                        <h6 class="entry-title"><a href="#">Linebacker Unit is the Bears' New Defensive Weakness</a></h6>
                    </div>
                </article>
            </li>
            <li>
                <article class="entry-item clearfix">
                    <div class="entry-thumb pull-left">
                        <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-24.jpg" alt="" /></a>
                    </div>
                    <div class="entry-content">
                        <h6 class="entry-title"><a href="#">This Week's Winner in My Favorite Place to Ride Photo Contest</a></h6>
                    </div>
                </article>
            </li>
        </ul>
        <!-- older-post -->

    </div>
    <!-- widget -->
    
</div>