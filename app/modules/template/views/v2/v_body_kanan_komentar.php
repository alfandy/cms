<div class="widget kopa-recent-comments-widget">
        
    <h2 class="widget-title widget-title-style-5">Recent comments</h2>

    <ul class="clearfix">
        
        <li>
            <article class="entry-item clearfix">
                <div class="entry-thumb">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/avatar/avatar-2.jpg" alt="" /></a>
                </div>
                <div class="entry-content">
                    <p><a href="#">Gloria</a> Agreed… Excellent composition, pov and light! Nam ut neque condimentum...</p>
                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                </div>
            </article>
        </li>

        <li>
            <article class="entry-item clearfix">
                <div class="entry-thumb">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/avatar/avatar-3.jpg" alt="" /></a>
                </div>
                <div class="entry-content">
                    <p><a href="#">Gloria</a> Agreed… Excellent composition, pov and light! Nam ut neque condimentum...</p>
                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                </div>
            </article>
        </li>

        <li>
            <article class="entry-item clearfix">
                <div class="entry-thumb">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/avatar/avatar-4.jpg" alt="" /></a>
                </div>
                <div class="entry-content">
                    <p><a href="#">Gloria</a> Agreed… Excellent composition, pov and light! Nam ut neque condimentum...</p>
                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                </div>
            </article>
        </li>

        <li>
            <article class="entry-item clearfix">
                <div class="entry-thumb">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/avatar/avatar-5.jpg" alt="" /></a>
                </div>
                <div class="entry-content">
                    <p><a href="#">Gloria</a> Agreed… Excellent composition, pov and light! Nam ut neque condimentum...</p>
                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                </div>
            </article>
        </li>

        <li>
            <article class="entry-item clearfix">
                <div class="entry-thumb">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/avatar/avatar-6.jpg" alt="" /></a>
                </div>
                <div class="entry-content">
                    <p><a href="#">Gloria</a> Agreed… Excellent composition, pov and light! Nam ut neque condimentum...</p>
                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                </div>
            </article>
        </li>

    </ul>

    <span class="widget-border-top"></span>

</div>