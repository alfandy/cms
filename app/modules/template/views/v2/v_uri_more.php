<div class="col-sm-3">

   
    <div class="widget widget_categories clearfix">
            
	    <div class="widget-title widget-title-style-3"><h6>Kategori</h6></div>
	    <?php if (!empty($list_kategori)): ?>
		    <ul>
		    	<?php foreach ($list_kategori as $l_k): ?>
			        <li><a href="<?php echo site_url('kategori-berita/'.$l_k->id.'/'.flag($l_k->nama))?>" title=""><?php echo $l_k->nama?></a></li>
		    	<?php endforeach ?>

		    </ul>
	    <?php endif ?>      


	</div>

    
</div>
<div class="col-sm-6">
		<?php  $this->load->view($modul .'/'.$view); ?> 	

</div>
<div class="col-sm-3" >
	<div class="widget kopa-recent-comments-widget">
        
	    <h2 class="widget-title widget-title-style-5">Berita Terbaru</h2>

	    <ul class="clearfix">
			<?php if (!empty($berita_t)): ?>
				   <?php foreach ($berita_t  as $b_t): ?>
				        <li>
				            <article class="entry-item clearfix">
				                <div class="">
				                    <p><a href="<?php echo site_url('content/'.$b_t->id.'/'.flag($b_t->judul))?>"><?php echo $b_t->judul?></a></p>
				                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left"><?php echo ts($b_t->ts)?></span></span>
				                </div>
				            </article>
				        </li>
				   <?php endforeach ?>
		    <?php endif ?>	    
	        

	        

	    </ul>

	    <span class="widget-border-top"></span>

	</div>
</div>
