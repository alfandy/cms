<link href="//vjs.zencdn.net/5.4.6/video-js.min.css" rel="stylesheet">
<script src="//vjs.zencdn.net/5.4.6/video.min.js"></script>
<div class="widget kopa-media-widget">
        
    <h2 class="widget-title widget-title-style-5">Video</h2>

    <article class="last-item">
        <div id="videoUpdateGet">
            <?php if (!empty($videoT1)): ?>
                <div class="entry-thumb">
                    <video id="video1" class="video-js vjs-default-skin"  style="width: 100%;" height="240" 
                        data-setup='{"controls" : true, "autoplay" : false, "preload" : "auto"}'>
                        <source src="<?= base_url('upload/video/' .$videoT1->file) ?>">
                    </video>
                </div>
                <div class="entry-content clearfix">
                    <h6 class="entry-title"><a href="#"><?php echo $videoT1->keterangan?></a></h6>
                    
                </div>
            <?php endif ?>
        </div>
    </article>

    <ul class="older-post clearfix">
        <?php if (!empty($videoT)): ?>
            <?php foreach ($videoT as $v_t): ?>
                <li onclick="getvideoPutar('<?php echo $v_t->id?>');" >
                    <h6 class="entry-title"><a ><?php echo $v_t->keterangan?></a></h6>
                    <a  class="play-icon"><i class="fa fa-play" ></i></a>
                </li>
            <?php endforeach ?>
        <?php endif ?>
    </ul>

    <span class="widget-border-top"></span>

</div>
<script>
    function getvideoPutar(aaa) {
        console.log(aaa)
        var data = aaa;
        $.ajax({
            url: '<?php echo site_url("video-get-data")?>',
            type: 'GET',
            dataType: 'html',
            data: {data: data},
            beforeSend : function(){

            },
            success : function(get){
                $('#videoUpdateGet').html(get);
            },
            error : function(){
                console.log('error');
            }
        });
        
    }
</script>