<?php
    $logo = base_url().'img/head/head.png';
    $abot = "We are a young and passionate news and development team from NY. We take pride in our work. Every feature was carefully chosen and designed to ease the way to that perfect news or magazine website.";
    $folow = '';
?>


<div id="bottom-sidebar">
        
    <div class="wrapper clearfix">

        <div id="bottom-logo">
            <a href="<?php echo site_url();?>"><img  style="width:100px; " src="<?php echo $logo;?>" alt="" /></a>
        </div>

        <div class="widget-area-4 pull-left">
            <?php if ($setingWeb->ket != ''): ?>
                <div class="widget widget_text">

                    <h5 class="widget-title widget-title-style-7">About us</h5>

                    <p><?php echo $setingWeb->ket;?></p>
                    
                </div>
            <?php endif ?>
            <!-- widget_text -->

            <div class="widget kopa-social-links-widget">
                <ul class="social-links clearfix">
                    <li>Follow us:</li>
                    <?php if ($setingWeb->fb != ''): ?>
                        <li>
                            <a href="<?php echo $setingWeb->fb ?>" class="fa fa-facebook"></a>
                            <div class="square-1"></div>
                            <div class="square-2"></div>
                        </li>
                    <?php endif ?>
                    <?php if ($setingWeb->tw != ''): ?>
                        <li>
                            <a href="<?php echo $setingWeb->tw ?>" class="fa fa-twitter"></a>
                            <div class="square-1"></div>
                            <div class="square-2"></div>
                        </li>
                    <?php endif ?>
                    <?php if ($setingWeb->g != ''): ?>
                        <li>
                            <a href="<?php echo $setingWeb->g ?>" class="fa fa-google-plus"></a>
                            <div class="square-1"></div>
                            <div class="square-2"></div>
                        </li>
                    <?php endif ?>
                    <?php if ($setingWeb->ig != ''): ?>
                        <li>
                            <a href="<?php echo $setingWeb->ig ?>" class="fa fa-instagram"></a>
                            <div class="square-1"></div>
                            <div class="square-2"></div>
                        </li>
                    <?php endif ?>
                </ul>
            </div>
           
            
        </div>
        <!-- widget-area-4 -->

        <div class="widget-area-5 pull-right">  
            <?php if ($setingWeb->nama != ''): ?>
                <div class="widget kopa-contact-info-widget">
                    
                    <h5 class="widget-title widget-title-style-8"><?php echo $setingWeb->nama?></h5>

                    <div class="contact-address"><?php echo $setingWeb->alamat?></div>
                    <div class="clearfix">
                        <a href="#" class="contact-phone pull-left"><?php echo $setingWeb->nomor?></a>
                        <a href="#" class="contact-email pull-left"><?php echo $setingWeb->email?></a>
                    </div>

                </div>
            <?php endif ?>
            <!-- widget -->

            
            <!-- widget -->
            
        </div>
        <!-- widget-area-5 -->

        <div class="clear"></div>

    </div>
    <!-- wrapper -->

</div>