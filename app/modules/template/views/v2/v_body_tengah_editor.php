<div class="widget kopa-nothumb-widget">

    <div class="widget-title widget-title-style-2">
        <h5>Editors pick</h5>

        <div class="widget-icon kopa-hex">
            <div class="square-1"><i class="fa fa-edit"></i></div>
            <div class="square-2"></div>
            <div class="square-3"></div>
        </div>
        <!-- widget-icon -->
    </div>
    <!-- widget-title -->

    <ul class="clearfix">
        
        <li>
            <article class="entry-item">
                <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                <h6 class="entry-title"><a href="#">Danger Zones for Blue Whales?</a></h6>
                <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
            </article>
        </li>

        <li>
            <article class="entry-item">
                <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                <h6 class="entry-title"><a href="#">Danger Zones for Blue Whales?</a></h6>
                <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
            </article>
        </li>

        <li>
            <article class="entry-item">
                <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                <h6 class="entry-title"><a href="#">Danger Zones for Blue Whales?</a></h6>
                <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
            </article>
        </li>

        <li>
            <article class="entry-item">
                <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                <h6 class="entry-title"><a href="#">Danger Zones for Blue Whales?</a></h6>
                <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
            </article>
        </li>

        <li>
            <article class="entry-item">
                <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                <h6 class="entry-title"><a href="#">Danger Zones for Blue Whales?</a></h6>
                <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
            </article>
        </li>

    </ul>

    <span class="icon-bottom"><i class="fa fa-angle-down"></i></span>
    
</div>