<div class="widget kopa-article-list-5-widget">

    <h2 class="widget-title widget-title-style-4">
        
        <span class="text-after">Random News</span>
    </h2>
    <!-- widget-title -->

    <ul class="clearfix">
        
        <li>
            <?php if (!empty($berita111kanan)): ?>
                <article class="entry-item">
                    <div class="entry-thumb">
                        <a href="<?php echo site_url('content/'.$berita111kanan->id.'/'.flag($berita111kanan->judul))?>"><img alt="" src="<?php echo base_url('upload/'.$berita111kanan->gambar)?>"></a>
                        <!-- <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a> -->
                        <!-- <a href="#" class="entry-comments">104</a> -->
                    </div>
                    <div class="entry-content">
                        <div class="meta-box">
                            
                            <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left"><?php echo ts($berita111kanan->ts)?></span></span>
                        </div>
                        <h6 class="entry-title"><a href="<?php echo site_url('content/'.$berita111kanan->id.'/'.flag($berita111kanan->judul))?>"><?php echo $berita111kanan->judul?></a></h6>
                    </div>
                </article>    
            <?php endif ?>
        </li>

    </ul>

    <span class="widget-border-top"></span>
    
</div>