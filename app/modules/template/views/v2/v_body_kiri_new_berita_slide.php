<style>
	.galleryItem a{
		display:block;
		position:relative;
		padding-bottom:50%;
		overflow:hidden;
		border-radius: 5px;
	}
	.galleryItem a img {
		position:absolute;
		width: 100%;
		height:auto;
		display:block;
	}
</style>
<div class="widget kopa-carousel-list-1-widget">

	<div class="owl-carousel kopa-carousel-1">
		<?php if (!empty($berita_3)): ?>
			<?php foreach ($berita_3 as $b_3): ?>

				<div class="item">

					<article class="entry-item">
						<div class="entry-thumb galleryItem">
							<a href="<?php echo site_url('content/'.$b_3->id.'/'.flag($b_3->judul))?>"><img src="<?php echo base_url().'upload/'.$b_3->gambar?>" alt="" /></a>
						</div>
						<!-- entry-thumb -->
						<div class="entry-content">

							<!-- <a href="#" class="entry-author">Fred Seibert:</a> -->
							<h2 class="entry-title"><a href="<?php echo site_url('content/'.$b_3->id.'/'.flag($b_3->judul))?>"><?php echo $b_3->judul?></a></h2>                              
						</div>
						<!-- entry-content -->
					</article>
					<!-- entry-item -->

				</div>
			<?php endforeach ?>
		<?php endif ?>

	</div>
	<!-- owl-carousel -->

</div>