<div class="widget kopa-article-list-1-widget">
    <?php if (!empty($berita_1)): ?>
        <article class="last-item">
            <div class="entry-thumb">
                <a href="<?php echo site_url('content/'.$berita_1->id.'/'.flag($berita_1->judul))?>"><img src="<?php echo base_url().'upload/'.$berita_1->gambar?>" alt="" /></a>
                <!-- <a href="#" class="entry-comments">173</a> -->
            </div>
            <!-- entry-thumb -->
            <div class="entry-content">
                <div class="entry-content-inner">
                    
                    <a href="<?php echo site_url('content/'.$berita_1->id.'/'.flag($berita_1->judul))?>" class="entry-author" style="font-size: 23px;background-color: rgba(0, 0, 0, 0.38);color: #fff;"><?php echo $berita_1->judul?></a>
                </div>
                <!-- entry-content-inner -->                                
            </div>
            <!-- entry-content -->
        </article>
    <?php endif ?>
    <!-- last-item -->

    <ul class="older-post clearfix">
        <?php if (!empty($berita_3)): ?>
            <?php foreach ($berita_3 as $b_3): ?>
                <li>
                    <article class="entry-item">
                        <div class="entry-thumb">
                            <a href="<?php echo site_url('content/'.$b_3->id.'/'.flag($b_3->judul))?>"><img class="img-responsive" src="<?php echo base_url().'upload/'.$b_3->gambar?>" alt="" /></a>
                            <!-- <a href="#" class="entry-comments">30</a> -->
                        </div>
                        <div class="entry-content">
                            <div class="meta-box">
                                <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left"><?php echo ts($b_3->ts)?></span></span>
                            </div>
                            <h6 class="entry-title"><a href="<?php echo site_url('content/'.$b_3->id.'/'.flag($b_3->judul))?>"><?php echo $b_3->judul?></a></h6>
                        </div>
                    </article>
                </li>
            <?php endforeach ?>
        <?php endif ?>
        
        
    </ul>
    <!-- older-post -->
    
</div>
