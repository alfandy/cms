<div class="widget kopa-article-list-2-widget">

    <h2 class="widget-title">
        <span class="text-title">Technology</span>
        <span class="border-top"></span>
        <span class="border-bottom"></span>
    </h2>
    <!-- widget-title -->

    <ul class="clearfix">
        
        <li>
            <article class="entry-item standard-post">
                <div class="entry-thumb">
                    <img src="<?php echo base_url()?>depan2/placeholders/post-image/post-8.jpg" alt="" />
                    <a href="#" class="entry-icon"><span><i></i></span></a>
                    <a href="#" class="entry-categories clearfix"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Technology</span></a>
                </div>
                <div class="entry-content">
                    <a class="entry-comments" href="#">32</a>
                    <h6 class="entry-title"><a href="#">Scientists invent new letters for the alphabet</a></h6>
                </div>
            </article>
        </li>

        <li>
            <article class="entry-item gallery-post">
                <div class="entry-thumb">
                    <img src="<?php echo base_url()?>depan2/placeholders/post-image/post-9.jpg" alt="" />
                    <a href="#" class="entry-icon"><span><i class="fa fa-play"></i></span></a>
                    <a href="#" class="entry-categories clearfix"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Technology</span></a>
                </div>
                <div class="entry-content">
                    <a class="entry-comments" href="#">52</a>
                    <h6 class="entry-title"><a href="#">Show Karen Lewis Might Also Help</a></h6>
                </div>
            </article>
        </li>

        <li>
            <article class="entry-item video-post">
                <div class="entry-thumb">
                    <img src="<?php echo base_url()?>depan2/placeholders/post-image/post-10.jpg" alt="" />
                    <a href="#" class="entry-icon"><span><i class="fa fa-play"></i></span></a>
                    <a href="#" class="entry-categories clearfix"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Technology</span></a>
                </div>
                <div class="entry-content">
                    <a class="entry-comments" href="#">17</a>
                    <h6 class="entry-title"><a href="#">What's Next for Chicago's Remaining</a></h6>
                </div>
            </article>
        </li>

    </ul>
    
</div>