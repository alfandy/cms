<!DOCTYPE html>
<html lang="en">

<head>

    <?php $this->load->view('v_meta');?>

</head>

<body class="kopa-home-1">

    <header class="kopa-page-header" >

        <?php $this->load->view('v_top1_runingtax');?>
        <!-- kopa-header-top -->

        <?php $this->load->view('v_top2_img');?>
        <!-- kopa-header-middle -->

        <?php $this->load->view('v_top3_nav1');?>
        <!-- kopa-header-bottom -->
        <?php //$this->load->view('v_top4_nav2'); ?>

        <!-- secondary-nav -->

    </header>
    <!-- kopa-page-header -->

    <?php //$this->load->view('v_top5_slide');?>
    <!-- kopa-home-slider-box -->


    <?php  $this->load->view('v_body')?>
    <!-- main-content -->

    <?php $this->load->view('v_bot');?>
    <!-- bottom-sidebar -->

    <?php $this->load->view('v_foot')?>
    <!-- kopa-page-footer -->


    <?php $this->load->view('v_js')?>
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.10&appId=371822583252380";
          fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
  </script>

</body>
</html>
