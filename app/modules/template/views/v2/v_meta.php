<?php
    $title = "MultimediaNews";
    $icon  = base_url('img/icon/icon.png');
?>

<meta charset="utf-8">
<title><?php echo $title?></title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<?php if (!empty($lconten)): ?>
    <meta property="og:title" content="<?php echo $lconten->judul ?>">
    <meta property="og:description" content="<?php echo strip_tags(substr($lconten->isi, 0 , 100))?>">
    <meta property="og:image" content="<?php echo base_url().'upload/'.$lconten->gambar?>">
    <meta property="og:url" content="<?php echo curPageURL();?>">
<?php endif ?>
<!-- <style>
    @import url(http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700,300italic);
    @import url(http://fonts.googleapis.com/css?family=Lato:400,300,700);
</style> -->
<link rel="stylesheet" href="<?php echo base_url()?>depan2/css/bootstrap.css" media="all" />
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link rel="stylesheet" href="<?php echo base_url()?>depan2/css/superfish.css" media="all" />
<link rel="stylesheet" href="<?php echo base_url()?>depan2/css/owl.carousel.css" media="all" />
<link rel="stylesheet" href="<?php echo base_url()?>depan2/css/owl.theme.css" media="all" />
<link rel="stylesheet" href="<?php echo base_url()?>depan2/css/jquery.navgoco.css"/>
<link rel="stylesheet" href="<?php echo base_url()?>depan2/css/flexslider.css" media="all" />
<link rel="stylesheet" href="<?php echo base_url()?>depan2/style.css">
<style>
	.kopa-header-bottom.fixed {
	    position: fixed;
	    top: 0;
	    width: 100%;
	    max-height: 99px;
	    background-color: #191919;
	    z-index: 1000;
	    padding: 0 0;
	}

	#main-nav {
	    padding: 25px 39px 25px 200px;
	    margin-top: -20px;
	    max-width: 1239px;
	}
</style>
<link rel="stylesheet" href="<?php echo base_url()?>depan2/css/responsive.css"/>
<script src="<?php echo base_url()?>depan2/js/modernizr.custom.js"></script>

<!-- Le fav and touch icons -->
<link rel="shortcut icon" href="<?php echo $icon ?>">
<link rel="apple-touch-icon" href="<?php echo base_url()?>depan2/img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url()?>depan2/img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url()?>depan2/img/apple-touch-icon-114x114.png">
