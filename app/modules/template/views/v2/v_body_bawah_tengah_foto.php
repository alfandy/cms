<div class="widget kopa-slider-with-thumb-widget">
        
    <div class="flexslider kopa-slider-width-thumb">
    
        <ul class="slides">
            
            <li>
                <article class="entry-item">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-16.jpg" alt="" /></a>
                    <div class="entry-content kopa-hex">
                        <div class="square-1">
                            <span class="event-date">04. 29. 14</span>
                            <h6 class="event-title">Photography</h6>
                            <a href="#">Views gallery</a>
                        </div>
                        <div class="square-2"></div>
                        <div class="square-3"></div>
                    </div>
                </article>
            </li>

            <li>
                <article class="entry-item">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-16.jpg" alt="" /></a>
                    <div class="entry-content kopa-hex">
                        <div class="square-1">
                            <span class="event-date">04. 29. 14</span>
                            <h6 class="event-title">Photography</h6>
                            <a href="#">Views gallery</a>
                        </div>
                        <div class="square-2"></div>
                        <div class="square-3"></div>
                    </div>
                </article>
            </li>

            <li>
                <article class="entry-item">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-16.jpg" alt="" /></a>
                    <div class="entry-content kopa-hex">
                        <div class="square-1">
                            <span class="event-date">04. 29. 14</span>
                            <h6 class="event-title">Photography</h6>
                            <a href="#">Views gallery</a>
                        </div>
                        <div class="square-2"></div>
                        <div class="square-3"></div>
                    </div>
                </article>
            </li>

            <li>
                <article class="entry-item">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-16.jpg" alt="" /></a>
                    <div class="entry-content kopa-hex">
                        <div class="square-1">
                            <span class="event-date">04. 29. 14</span>
                            <h6 class="event-title">Photography</h6>
                            <a href="#">Views gallery</a>
                        </div>
                        <div class="square-2"></div>
                        <div class="square-3"></div>
                    </div>
                </article>
            </li>

        </ul>

    </div>
    <!-- flexslider -->

    <div class="flexslider kopa-flex-carousel">

        <ul class="slides">
            
            <li>
                <span>01/</span> Best of July
            </li>

            <li>
                <span>02/</span> Smile! You're on Your Shot
            </li>

            <li>
                <span>03/</span> Your Best Wild Pictures
            </li>

            <li>
                <span>04/</span> Faces of Farming
            </li>

        </ul>
        
    </div>
    <!-- kopa-flex-carousel -->

</div>