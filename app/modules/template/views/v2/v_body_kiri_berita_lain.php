<div class="widget kopa-article-list-3-widget">

    <h5 class="widget-title">
        <span class="text-title" style="font-size: 20px;">Berita Kategori</span>
        <span class="border-top"></span>
        <span class="border-bottom"></span>
    </h5>
    <!-- widget-title -->

    <?php echo form_dropdown('kategori' , $kategori_berita , '' , 'class="form-control" style="margin-bottom: 20px" onchange="pilihKategoriBerita(this)"'); ?>
        
    <div id="getBeritaKategori">
        <?php if (!empty($berita_kategori_awal)): ?>
            <ul class="clearfix">
                <?php foreach ($berita_kategori_awal as $b_k_a): ?>
                    <li>
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="<?php echo site_url('content/'.$b_k_a->id.'/'.flag($b_k_a->judul))?>"><img src="<?php echo base_url().'upload/'.$b_k_a->gambar?>" alt="" /></a>
                                <a href="<?php echo site_url('content/'.$b_k_a->id.'/'.flag($b_k_a->judul))?>" class="entry-categories clearfix"><i class=""></i><span class="pull-left"><?php echo $b_k_a->kategori?></span></a>
                                <!-- <a class="entry-comments" href="#">209</a> -->
                            </div>
                            <div class="entry-content">
                                <div class="meta-box">
                                   <!--  <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Price Peterson</a></span> -->
                                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left"><?php echo ts($b_k_a->ts)?></span></span>
                                </div>
                                <h6 class="entry-title"><a href="<?php echo site_url('content/'.$b_k_a->id.'/'.flag($b_k_a->judul))?>"><?php echo $b_k_a->judul?></a></h6>
                            </div>
                        </article>    
                    </li>
                <?php endforeach ?>
            </ul>
                
        <?php else: ?>
            <article class="entry-item" style="margin-top: 20px;">
                <center><h3>Belum Ada Berita Untuk Kategori Ini</h3></center>
            </article>    
            
        <?php endif ?>

    </div>   
    
</div>


<script>
    function pilihKategoriBerita(aaa) {
        var data = aaa.value;
        $.ajax({
            url: '<?php echo site_url("berita-kategori");?>',
            type: 'GET',
            dataType: 'html',
            data: {data: data},
            beforeSend : function() {
                
            },
            success : function(get) {
                console.log(data);
                $("#getBeritaKategori").html(get);
            },
            error : function(error) {
                console.log('error ' +error);
            }

        });
        
    }
</script>