<nav id="secondary-nav">

    <div class="wrapper">

        <ul id="secondary-menu" class="mega-menu clearfix">

            <li class="mega-menu-item">
                <a href="#">News + Blog</a>
                <div class="sf-mega clearfix">
                    <div class="sf-mega-section">
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-79.jpg" alt="" /></a>
                                <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                            </div>
                            <div class="entry-content">
                                <div class="meta-box">
                                    <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                </div>
                                <h4 class="entry-title"><a href="#">Two Ferguson Cops Accused of Hitting, Hog-Tying Children</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="sf-mega-section">
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-80.jpg" alt="" /></a>
                                <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                            </div>
                            <div class="entry-content">
                                <div class="meta-box">
                                    <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                </div>
                                <h4 class="entry-title"><a href="#">Man Casually Climbs Brooklyn Bridge to Snap Photos</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="sf-mega-section">
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-81.jpg" alt="" /></a>
                                <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                            </div>
                            <div class="entry-content">
                                <div class="meta-box">
                                    <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                </div>
                                <h4 class="entry-title"><a href="#">Jazz Legend Sonny Rollins: 'Onion' Writer Was Mean To Me</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="sf-mega-section">
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-82.jpg" alt="" /></a>
                                <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                            </div>
                            <div class="entry-content">
                                <div class="meta-box">
                                    <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                </div>
                                <h4 class="entry-title"><a href="#">Life in Pictures: Bob and Mike Bryan</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="clear"></div>   
                </div>
                <!-- sf-mega -->
            </li>
            <li>
                <a href="#">Video</a>
                <ul>
                    <li class="current-menu-item"><a href="index-1.html">Home style 1</a></li>
                    <li><a href="index-2.html">Home style 2</a></li>
                    <li><a href="index-3.html">Home style 3</a></li>
                    <li><a href="index-4.html">Home style 4</a></li>
                </ul>
            </li>
            <li><a href="#">World</a></li>
            <li><a href="#">Movies</a></li>
            <li>
                <a href="#">Fashion</a>
                <div class="sf-mega clearfix">
                    <div class="sf-mega-section">
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-79.jpg" alt="" /></a>
                                <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                            </div>
                            <div class="entry-content">
                                <div class="meta-box">
                                    <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                </div>
                                <h4 class="entry-title"><a href="#">Two Ferguson Cops Accused of Hitting, Hog-Tying Children</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="sf-mega-section">
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-80.jpg" alt="" /></a>
                                <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                            </div>
                            <div class="entry-content">
                                <div class="meta-box">
                                    <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                </div>
                                <h4 class="entry-title"><a href="#">Man Casually Climbs Brooklyn Bridge to Snap Photos</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="sf-mega-section">
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-81.jpg" alt="" /></a>
                                <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                            </div>
                            <div class="entry-content">
                                <div class="meta-box">
                                    <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                </div>
                                <h4 class="entry-title"><a href="#">Jazz Legend Sonny Rollins: 'Onion' Writer Was Mean To Me</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="sf-mega-section">
                        <article class="entry-item">
                            <div class="entry-thumb">
                                <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-82.jpg" alt="" /></a>
                                <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                            </div>
                            <div class="entry-content">
                                <div class="meta-box">
                                    <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                    <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                </div>
                                <h4 class="entry-title"><a href="#">Life in Pictures: Bob and Mike Bryan</a></h4>
                            </div>
                        </article>
                    </div>
                    <div class="clear"></div>   
                </div>
                <!-- sf-mega -->
            </li>
            <li><a href="#">Music</a></li>
            <li><a href="#">Reviews</a></li>
            <li><a href="#">Photo</a></li>
            
        </ul>
        <!-- secondary-menu -->

        <span class="secondary-mobile-label">Secondary Menu</span>
        
        <div class="secondary-mobile-menu-wrapper">
            <ul id="secondary-mobile-menu">
                <li class="current-menu-item">
                    <a href="#">News + Blog</a>
                    <ul class="sf-mega clearfix">
                        <li class="sf-mega-section">
                            <article class="entry-item">
                                <div class="entry-thumb">
                                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-79.jpg" alt="" /></a>
                                    <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                                </div>
                                <div class="entry-content">
                                    <div class="meta-box">
                                        <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                        <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                    </div>
                                    <h4 class="entry-title"><a href="#">Two Ferguson Cops Accused of Hitting, Hog-Tying Children</a></h4>
                                </div>
                            </article>
                        </li>
                        <li class="sf-mega-section">
                            <article class="entry-item">
                                <div class="entry-thumb">
                                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-80.jpg" alt="" /></a>
                                    <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                                </div>
                                <div class="entry-content">
                                    <div class="meta-box">
                                        <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                        <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                    </div>
                                    <h4 class="entry-title"><a href="#">Man Casually Climbs Brooklyn Bridge to Snap Photos</a></h4>
                                </div>
                            </article>
                        </li>
                        <li class="sf-mega-section">
                            <article class="entry-item">
                                <div class="entry-thumb">
                                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-81.jpg" alt="" /></a>
                                    <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                                </div>
                                <div class="entry-content">
                                    <div class="meta-box">
                                        <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                        <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                    </div>
                                    <h4 class="entry-title"><a href="#">Jazz Legend Sonny Rollins: 'Onion' Writer Was Mean To Me</a></h4>
                                </div>
                            </article>
                        </li>
                        <li class="sf-mega-section">
                            <article class="entry-item">
                                <div class="entry-thumb">
                                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-82.jpg" alt="" /></a>
                                    <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                                </div>
                                <div class="entry-content">
                                    <div class="meta-box">
                                        <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                        <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                    </div>
                                    <h4 class="entry-title"><a href="#">Life in Pictures: Bob and Mike Bryan</a></h4>
                                </div>
                            </article>
                        </li>  
                    </ul>
                    <!-- sf-mega -->
                </li>
                <li>
                    <a href="#">Video</a>
                    <ul>
                        <li><a href="#">Video 1</a></li>
                        <li><a href="#">Video 2</a></li>
                        <li><a href="#">Video 3</a></li>
                    </ul>
                </li>
                <li><a href="#">World</a></li>
                <li><a href="#">Movies</a></li>
                <li>
                    <a href="#">Fashion</a>
                    <ul class="sf-mega clearfix">
                        <li class="sf-mega-section">
                            <article class="entry-item">
                                <div class="entry-thumb">
                                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-79.jpg" alt="" /></a>
                                    <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                                </div>
                                <div class="entry-content">
                                    <div class="meta-box">
                                        <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                        <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                    </div>
                                    <h4 class="entry-title"><a href="#">Two Ferguson Cops Accused of Hitting, Hog-Tying Children</a></h4>
                                </div>
                            </article>
                        </li>
                        <li class="sf-mega-section">
                            <article class="entry-item">
                                <div class="entry-thumb">
                                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-80.jpg" alt="" /></a>
                                    <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                                </div>
                                <div class="entry-content">
                                    <div class="meta-box">
                                        <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                        <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                    </div>
                                    <h4 class="entry-title"><a href="#">Man Casually Climbs Brooklyn Bridge to Snap Photos</a></h4>
                                </div>
                            </article>
                        </li>
                        <li class="sf-mega-section">
                            <article class="entry-item">
                                <div class="entry-thumb">
                                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-81.jpg" alt="" /></a>
                                    <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                                </div>
                                <div class="entry-content">
                                    <div class="meta-box">
                                        <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                        <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                    </div>
                                    <h4 class="entry-title"><a href="#">Jazz Legend Sonny Rollins: 'Onion' Writer Was Mean To Me</a></h4>
                                </div>
                            </article>
                        </li>
                        <li class="sf-mega-section">
                            <article class="entry-item">
                                <div class="entry-thumb">
                                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-82.jpg" alt="" /></a>
                                    <a class="entry-categories clearfix" href="#"><i class="fa fa-file-text-o pull-left"></i><span class="pull-left">Lifestyle</span></a>
                                </div>
                                <div class="entry-content">
                                    <div class="meta-box">
                                        <span class="entry-author clearfix"><span class="pull-left">By&nbsp;</span><a class="pull-left" href="#">Konzept &amp; art direction</a></span>
                                        <span class="entry-date clearfix"><i class="fa fa-calendar-o pull-left"></i><span class="pull-left">July 8, 2014</span></span>
                                    </div>
                                    <h4 class="entry-title"><a href="#">Life in Pictures: Bob and Mike Bryan</a></h4>
                                </div>
                            </article>
                        </li>  
                    </ul>
                    <!-- sf-mega -->
                </li>
                <li><a href="#">Music</a></li>
                <li><a href="#">Reviews</a></li>
                <li><a href="#">Photo</a></li>
            </ul>
            <!-- mobile-menu -->
        </div>
        <!-- secondary-mobile-menu-wrapper -->
        
    </div>
    <!-- wrapper -->
    
</nav>