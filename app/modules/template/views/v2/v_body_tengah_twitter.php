<div class="widget kopa-twitter-widget">
        
    <div class="widget-title widget-title-style-2">
        <h5>latest Twitter</h5>

        <div class="widget-icon kopa-hex">
            <div class="square-1"><i class="fa fa-twitter"></i></div>
            <div class="square-2"></div>
            <div class="square-3"></div>
        </div>
        <!-- widget-icon -->
    </div>
    <!-- widget-title -->                        

    <ul class="clearfix">
        
        <li>
            <div class="twitter-item">
                <p><a href="#">@yeahmahasiswa</a> kalau mukanya cakep tapi panuan, gimana minho? <span class="tweet-time">14 hours ago</span></p>
            </div>
        </li>

        <li>
            <div class="twitter-item">
                <p><a href="#">@yeahmahasiswa</a> kalau mukanya cakep tapi panuan, gimana minho? <span class="tweet-time">2 hours ago</span></p>
            </div>
        </li>

        <li>
            <div class="twitter-item">
                <p><a href="#">@yeahmahasiswa</a> kalau mukanya cakep tapi panuan, gimana minho? <span class="tweet-time">2 hours ago</span></p>
            </div>
        </li>

    </ul>

    <span class="icon-bottom"><i class="fa fa-angle-down"></i></span>

</div>