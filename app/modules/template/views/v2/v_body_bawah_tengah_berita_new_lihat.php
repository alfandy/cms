<div class="widget kopa-article-list-6-widget">
        
    <h2 class="widget-title">
        <span class="text-title">Latest Reviews</span>
        <span class="border-top"></span>
        <span class="border-bottom"></span>
    </h2>
    <!-- widget-title -->

    <div class="owl-carousel kopa-carousel-2">

        <div class="item">
            <article class="entry-item">
                <div class="entry-thumb">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-17.jpg" alt="" /></a>
                    <div class="mask"></div>
                </div>
                <div class="entry-content">
                    <span>Review</span>
                    <h6 class="entry-title"><a href="#">HOHOKUM REVIEW: ALL SIZZLE</a></h6>
                    <a href="#" class="entry-author">Philip Kollar</a>
                </div>
                <div class="entry-point kopa-hex-large">
                    <div class="square-1"></div>
                    <div class="square-2"></div>
                    <div class="square-3"></div>
                    <div class="kopa-hex">
                        <div class="square-1">8</div>
                        <div class="square-2"></div>
                        <div class="square-3"></div>
                    </div>   
                </div>
            </article>
        </div>
        <!-- item -->

        <div class="item">
            <article class="entry-item">
                <div class="entry-thumb">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-18.jpg" alt="" /></a>
                    <div class="mask"></div>
                </div>
                <div class="entry-content">
                    <span>Review</span>
                    <h6 class="entry-title"><a href="#">ARMA 3 REVIEW UPDATE: SINGLE-PLAYER, ZEUS MODULE AND MORE</a></h6>
                    <a href="#" class="entry-author">Charlie Hall</a>
                </div>
                <div class="entry-point kopa-hex-large">
                    <div class="square-1"></div>
                    <div class="square-2"></div>
                    <div class="square-3"></div>
                    <div class="kopa-hex">
                        <div class="square-1">6.5</div>
                        <div class="square-2"></div>
                        <div class="square-3"></div>
                    </div>   
                </div>
            </article>
        </div>
        <!-- item -->

        <div class="item">
            <article class="entry-item">
                <div class="entry-thumb">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-19.jpg" alt="" /></a>
                    <div class="mask"></div>
                </div>
                <div class="entry-content">
                    <span>Review</span>
                    <h6 class="entry-title"><a href="#">DIVINITY: ORIGINAL SIN REVIEW: NEXT TO GODLINESS</a></h6>
                    <a href="#" class="entry-author">Philip Kollar</a>
                </div>
                <div class="entry-point kopa-hex-large">
                    <div class="square-1"></div>
                    <div class="square-2"></div>
                    <div class="square-3"></div>
                    <div class="kopa-hex">
                        <div class="square-1">7.5</div>
                        <div class="square-2"></div>
                        <div class="square-3"></div>
                    </div>   
                </div>
            </article>
        </div>
        <!-- item -->

        <div class="item">
            <article class="entry-item">
                <div class="entry-thumb">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-20.jpg" alt="" /></a>
                    <div class="mask"></div>
                </div>
                <div class="entry-content">
                    <span>Review</span>
                    <h6 class="entry-title"><a href="#">DIVINITY: ORIGINAL SIN REVIEW: NEXT TO GODLINESS</a></h6>
                    <a href="#" class="entry-author">Philip Kollar</a>
                </div>
                <div class="entry-point kopa-hex-large hight-point">
                    <div class="square-1"></div>
                    <div class="square-2"></div>
                    <div class="square-3"></div>
                    <div class="kopa-hex">
                        <div class="square-1">8.5</div>
                        <div class="square-2"></div>
                        <div class="square-3"></div>
                    </div>   
                </div>
            </article>
        </div>
        <!-- item -->

        <div class="item">
            <article class="entry-item">
                <div class="entry-thumb">
                    <a href="#"><img src="<?php echo base_url()?>depan2/placeholders/post-image/post-18.jpg" alt="" /></a>
                    <div class="mask"></div>
                </div>
                <div class="entry-content">
                    <span>Review</span>
                    <h6 class="entry-title"><a href="#">ARMA 3 REVIEW UPDATE: SINGLE-PLAYER, ZEUS MODULE AND MORE</a></h6>
                    <a href="#" class="entry-author">Charlie Hall</a>
                </div>
                <div class="entry-point kopa-hex-large">
                    <div class="square-1"></div>
                    <div class="square-2"></div>
                    <div class="square-3"></div>
                    <div class="kopa-hex">
                        <div class="square-1">7</div>
                        <div class="square-2"></div>
                        <div class="square-3"></div>
                    </div>   
                </div>
            </article>
        </div>
        <!-- item -->
        
    </div>
    <!-- owl-carousel -->

</div>