<div class="widget kopa-direction-vertical-widget">
        
    <h2 class="widget-title">
        <span class="text-title">The latest NEWS</span>
        <span class="border-top"></span>
        <span class="border-bottom"></span>
    </h2>
    <!-- widget-title -->

    <div class="flexslider kopa-direction-vertical-slider">

        <ul class="slides">
            
            <li class="clearfix">
                <div class="entry-thumb pull-left">
                    <img src="<?php echo base_url()?>depan2/placeholders/post-image/post-7.jpg" alt="" />
                    <a href="#" class="entry-icon"><span><i class="fa fa-play"></i></span></a>
                </div>
                <ul class="pull-left">
                    <li>
                        <span>12h40 :</span>
                        <a href="#">Scientists invent new letters for the alphabet of life</a>
                    </li>
                    <li>
                        <span>11h04 :</span>
                        <a href="#">Five Easy Ways To Brighten Up Your Kitchen</a>
                    </li>
                    <li>
                        <span>10h25 :</span>
                        <a href="#">Scientists invent new letters for the alphabet of life</a>
                    </li>
                </ul>
            </li>

            <li class="clearfix">
                <div class="entry-thumb pull-left">
                    <img src="<?php echo base_url()?>depan2/placeholders/post-image/post-7.jpg" alt="" />
                    <a href="#" class="entry-icon"><span><i class="fa fa-play"></i></span></a>
                </div>
                <ul class="pull-left">
                    <li>
                        <span>12h40 :</span>
                        <a href="#">Scientists invent new letters for the alphabet of life</a>
                    </li>
                    <li>
                        <span>11h04 :</span>
                        <a href="#">Five Easy Ways To Brighten Up Your Kitchen</a>
                    </li>
                    <li>
                        <span>10h25 :</span>
                        <a href="#">Scientists invent new letters for the alphabet of life</a>
                    </li>
                </ul>
            </li>

        </ul>
        
    </div>
    <!-- kopa-direction-vertical-slider -->

</div>