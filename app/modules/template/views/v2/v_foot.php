<?php 
	$copy = "Divisi Humas Polri. All rights reserved. Terms of use & privacy policy";
 ?>
<footer id="kopa-page-footer">

    <div class="wrapper clearfix">
        
        <p id="copyright" class="pull-left">Copyright <?php echo date('Y')?> - <?php echo $copy;?></p>

        <p id="back-top" class="pull-right">
            <a href="#top" class="clearfix"><span class="pull-left back-top-text">Back to top</span><span class="back-top-icon pull-left"><i class="fa fa-angle-double-up"></i></span></a>
        </p>

    </div>
    <!-- wrapper -->

</footer>