<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('v_meta_head')?>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <?php $this->load->view('v_header')?>
      <!-- Left side column. contains the logo and sidebar -->
      <?php $this->load->view('v_menu_kiri')?>

      <!-- Content Wrapper. Contains page content -->
      <?php $this->load->view('v_body'); ?>
      
      <?php $this->load->view('v_footer');?>

      <!-- Control Sidebar -->
      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <?php $this->load->view('v_pengaturan')?>
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <?php $this->load->view('v_js');?>
  </body>
</html>
