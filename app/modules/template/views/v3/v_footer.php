<?php
	$version = "1.0.0";
	$by_a = "C55";
?>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version Beta</b> <?php echo $version;?> 
    </div>
    <strong>Copyright &copy; <?php echo date('Y');?> - <?php echo date('Y')+1;?> <a href="http://almsaeedstudio.com"><?php echo $by_a; ?></a>.</strong> All rights reserved.
  </footer>