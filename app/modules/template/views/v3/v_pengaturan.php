<aside class="control-sidebar control-sidebar-dark">
  <!-- Create the tabs -->
  <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
    <li class="active"><a href="#control-sidebar-settings-tab"  data-toggle="tab"><i class="fa fa-gears"></i></a></li>
  </ul>
  <!-- Tab panes -->
  <div class="tab-content">
    <!-- Home tab content -->
    <!-- Stats tab content -->
    <div class="tab-pane " id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
    <!-- Settings tab content -->
    <div class="tab-pane active" id="control-sidebar-settings-tab">
      <?php echo form_open('', 'id="setingWeb" class="form-horizontal"' );?>
        <h3 class="control-sidebar-heading">General Settings</h3>
        <div class="form-group">
          <div class="col-sm-12">
            <label class="control-label">
              Nama Web
            </label>
          </div>
          <div class="col-sm-12">
            <input type="text" placeholder="Nama Web" value="<?php echo $setingWeb->nama?>" class="form-control" id="namaWeb" onblur="simpanSetingWeb()">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <label class="control-label">
              About
            </label>
          </div>
          <div class="col-sm-12">
            <textarea  id="abotWeb" class="form-control" onblur="simpanSetingWeb()">
              <?php echo $setingWeb->ket?>
            </textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <label class="control-label">
              Alamat
            </label>
          </div>
          <div class="col-sm-12">
            <textarea  id="alamatWeb" class="form-control"  onblur="simpanSetingWeb()">
              <?php echo $setingWeb->alamat?>
            </textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <label class="control-label">
              Nomor Kontak
            </label>
          </div>
          <div class="col-sm-12">
            <input type="text" placeholder="Nomor Kontak" class="form-control" id="nomorWeb" value="<?php echo $setingWeb->nomor?>" onblur="simpanSetingWeb()">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <label class="control-label">
              Facebook
            </label>
          </div>
          <div class="col-sm-12">
            <input type="text" placeholder="Facebook" class="form-control" id="fbWeb" value="<?php echo $setingWeb->fb?>" onblur="simpanSetingWeb()">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <label class="control-label">
              Twiter
            </label>
          </div>
          <div class="col-sm-12">
            <input type="text" placeholder="Twitter" class="form-control" id="twWeb" value="<?php echo $setingWeb->tw?>" onblur="simpanSetingWeb()">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <label class="control-label">
              Google+
            </label>
          </div>
          <div class="col-sm-12">
            <input type="text" placeholder="Google" class="form-control" id="gWeb" value="<?php echo $setingWeb->g?>" onblur="simpanSetingWeb()">
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <label class="control-label">
              Instagram
            </label>
          </div>
          <div class="col-sm-12">
            <input type="text" placeholder="Instagram" class="form-control" id="igWeb" value="<?php echo $setingWeb->ig?>" onblur="simpanSetingWeb()">
          </div>
        </div>
        <?php echo form_close(); ?>
    </div><!-- /.tab-pane -->
  </div>
</aside><!-- /.control-sidebar -->


<script>
  function simpanSetingWeb(){
    var namaWeb =  $("#namaWeb").val();
    var abotWeb = $("#abotWeb").val();
    var alamatWeb = $("#alamatWeb").val();
    var nomorWeb = $("#nomorWeb").val();
    var fbWeb = $("#fbWeb").val();
    var twWeb = $("#twWeb").val();
    var gWeb = $("#gWeb").val();
    var igWeb = $("#igWeb").val();

    $.ajax({
      url: '<?php echo site_url("cpanel/seting/update")?>',
      type: 'POST',
      data: {
        namaWeb: namaWeb,
        abotWeb : abotWeb,
        alamatWeb : alamatWeb,
        nomorWeb : nomorWeb,
        fbWeb : fbWeb,
        twWeb : twWeb,
        gWeb : gWeb,
        igWeb : igWeb
      },
      beforeSend : function(){

      },
      success : function(get){
        $.toaster({priority: 'success', title: 'Success', message: 'Berhasil Update Data Web'});
      },
      error : function(){
        console.log(error);
      }
    });
    
  }
</script>