<?php
$user = $this->session->userdata('user_login');
$user_login = $this->session->userdata('user_login');
$waktuDaftar = "";
$icon  = base_url('img/icon/user.png');
?>

<header class="main-header">
  <!-- Logo -->
  <a href="<?php echo site_url() ?>" class="logo">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><b>C</b>PNL</span>
    <!-- logo for regular state and mobile devices -->
    <span class="logo-lg"><b>CMS</b>PANEL</span>
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
        
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <img src="<?php echo $icon ?>" class="user-image" alt="User Image">
            <span class="hidden-xs"><?php echo $user?></span>
          </a>
          <ul class="dropdown-menu">
            <!-- User image -->
            <li class="user-header">
              <img src="<?php echo $icon ?>" class="img-circle" alt="User Image">
              <p>
                <?php echo $user;  ?>
                <small><?php echo $waktuDaftar?></small>
              </p>
            </li>

            <li class="user-footer">
              <div class="pull-left">
                <button class="btn-default btn-flat btn" data-toggle="modal" data-target="#exampleModalaaaaaaaaaaaaaaaaaa"><i class="fa fa-key"></i> Ganti Password</button>                    
              </div>
              <div class="pull-right">
                <a href="<?php echo site_url('keluar')?>" class="btn btn-default btn-flat">Sign out</a>
              </div>
            </li>
          </ul>
        </li>
        <!-- Control Sidebar Toggle Button -->
        <li>
          <a href="#" data-toggle="control-sidebar"  ><i class="fa fa-gears"></i></a>
        </li>
      </ul>
    </div>
  </nav>
</header>


<div class="modal fade" id="exampleModalaaaaaaaaaaaaaaaaaa"  role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Update User</h4>
      </div>
      <div class="isi"><p><form class="form-horizontal" id="user-form"  method="post">
        <div class="modal-body"> 

          <div class="form-group">
            <label class="col-sm-3 control-label required" for="User_username">Username <span class="required">*</span></label>
            <div class="col-sm-9">
              <input readonly="readonly" class="span5 form-control" maxlength="225" placeholder="Username" name="User[username]" id="User_username" type="text" value="<?php echo $this->session->userdata('user_login')?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label required" for="User_old_password">Old Password <span class="required">*</span></label><div class="col-sm-9">
              <input class="span5 form-control" maxlength="225" placeholder="Old Password" name="passworLama" id="passworLama" type="password" onblur="cekPassLama(this)">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label required" for="User_new_password">New Password <span class="required">*</span></label>
            <div class="col-sm-9">
              <input class="span5 form-control" maxlength="225"  placeholder="New Password"  id="passbarukwa" type="password">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label required" for="User_repeat_password">Repeat Password <span class="required">*</span></label>
            <div class="col-sm-9">
              <input class="span5 form-control" maxlength="225" placeholder="Repeat Password"  id="ulangpasbro" type="password" onblur="cekSamaKwa(this)">
            </div>
          </div>   
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
          <button class="btn btn-primary"  type="button" onclick="simpanPassBeruBro('<?php echo $user_login ?>');"><i class="fa fa-check-square-o"></i> Update Data</button>
        </div>
      </form>

    </p>
  </div>
    </div>
  </div>
</div>


<script>

  function simpanPassBeruBro(username){

    var passworLama = $("#passworLama");
    var passbarukwa = $("#passbarukwa");
    var ulangpasbro = $("#ulangpasbro");
    if (passworLama.val() == '') {
      passworLama.focus();
      $.toaster({priority: 'danger', title: 'Info', message: 'Maaf Password Lama Tidak Bisa Kosong'});
    }
    else if(passbarukwa.val() == ''){
      passbarukwa.focus();
      $.toaster({priority: 'danger', title: 'Info', message: 'Maaf Password Baru Tidak Bisa Kosong'});
    }
    else if(ulangpasbro.val() == ''){
      ulangpasbro.focus();
      $.toaster({priority: 'danger', title: 'Info', message: 'Masukn kembali password Baru'});
    }else{
      $.ajax({
        url: '<?php echo site_url("cpanel/user/update-password")?>',
        type: 'POST',
        dataType: 'json',
        data: {usr1 : username ,p1: passworLama.val() , p2 : passbarukwa.val() , p3 : ulangpasbro.val()},
        beforeSend : function(){

        },
        success : function(get){
          // alert(get);
          console.log(get);
          if (get.stat == 'success') {
            passworLama.val('');
            passbarukwa.val('');
            ulangpasbro.val('');
          }
            $.toaster({priority: get.stat, title: 'Info', message: get.pesan});
        },
        error : function(){
          console.log('errror wkwkwkwkwk');
        }
      });
      
    }
  }

  function cekSamaKwa(arrra){
    var cekNo = arrra.value;
    var pass = $("#passbarukwa").val();
    if (cekNo == pass) {
       $.toaster({priority: 'info', title: 'Info', message: 'Password Cocok'});
    }else {
      $.toaster({priority: 'info', title: 'Info', message: 'Password tidak Cocok'});
    }
  }

  function  cekPassLama(argument) {
    var passworLama = argument.value;
    if (passworLama == '') {

    }else{

      $.ajax({
        url: '<?php echo site_url("cpanel/user/cek-user-lamakwa")?>',
        type: 'GET',
        dataType: 'html',
        data: {data: passworLama},
        beforeSend : function(){

        },
        success : function(get){
          // alert(get);
          $.toaster({priority: 'info', title: 'Info', message: get});
        },
        error : function(){
          console.log('errror wkwkwkwkwk');
        }
      });
    }
    
  }
</script>