<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Seting extends MX_Controller {

    private $modul = '';
    private $redirect = '';

    public function __construct() {
        parent::__construct();
        if (!$this->autentifikasi->sudah_login())
            redirect('cpanel/login-cpnl','refresh');
        $this->modul = 'seting'; //nama modul
        $this->redirect = ''; //base url;
    }


    public function update_web(){
        if ($this->input->post()) {
            // namaWeb: namaWeb,
            // abotWeb : abotWeb,
            // nomorWeb : nomorWeb,
            // alamatWeb : alamatWeb,
            // fbWeb : fbWeb,
            // twWeb : twWeb,
            // gWeb : gWeb,
            // igWeb : igWeb
            $namaWeb = $this->input->post('namaWeb');
            $abotWeb = $this->input->post('abotWeb');
            $nomorWeb = $this->input->post('nomorWeb');
            $alamatWeb = $this->input->post('alamatWeb');
            $fbWeb = $this->input->post('fbWeb');
            $twWeb = $this->input->post('twWeb');
            $gWeb = $this->input->post('gWeb');
            $igWeb = $this->input->post('igWeb');


            $data = array(
                'nama' => $namaWeb,
                'ket' => $abotWeb,
                'nomor' => $nomorWeb,
                'alamat' => $alamatWeb,
                'fb' => $fbWeb,
                'tw' => $twWeb,
                'g' => $gWeb,
                'ig' => $igWeb
            );
            $this->m_seting->update('1' , $data);
        }else{
            redirect('','refresh');
        }
    }

    public function index() {
        $data['judul1'] = 'Seting';
        $data['judul2'] = 'Setingan';
        $data['view'] = 'seting/list';
        $data['modul'] = $this->modul;
        echo Modules::run('template/belakan', $data);
    }

    public function vtambah(){
        $this->load->view('tambah_runing');
    }

    public function simpan(){
       if ($this->input->post()) {
           $data = array(
                'id' => auto_inc('m_runing' , 'id'),
                'nama' => $this->input->post('nama'),
                'status' => '1',
                'hapus' => ''
           );
           $this->m_runing->insert($data);
       }else{
            redirect('','refresh');
       }
    }

    public function hapus(){
        if ($this->input->post()) {
            $id =  $this->input->post('data');
            $data = array(
                'hapus' => date_timestamp_get(date_create()),
            );
            $this->m_runing->update($id ,$data);
        }else{
            redirect('','refresh');
        }
    }

    

    public function vupdate(){
        if ($this->input->get('data')) {
            $id = $this->input->get('data');

            $data['lu'] = $this->m_runing->get_by(array('id' => $id));
            $this->load->view('update_runing' , $data);
        }else{
            redirect('','refresh');
        }
    }

    public function update(){
        if ($this->input->post()) {
            $id = $this->input->post('id');
            $data = array(
                "nama" => $this->input->post('nama')        
            );
            $this->m_runing->update($id , $data);
        }else{
            redirect('','refresh');
        }
    }

    public function updatestatus(){
        if ($this->input->post('data')) {
            $id = $this->input->post('data');
            $status = $this->input->post('status');
            $data = array(
                'status' => $status
            );

            $this->m_runing->update($id , $data);
        }else{
            redirect('','refresh');
        }
    }
}
